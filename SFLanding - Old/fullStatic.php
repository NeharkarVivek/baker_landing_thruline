<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 */

get_header(); ?>

<div class="head">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="/wp-content/themes/bakerLanding/slices\Baker College Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    <div id="SlideInfo">
      <h1>Write your own future</h1>
      <img src="/wp-content/themes/bakerLanding/slices/Video Ply BTN.png" alt="Play Video">
      <p>Quostecta quostecta eaqui bla quis dolupta nones vendam nones et as nonsequamus. Explant quostecta eaqui bla quis vendam nones et as nonsequamus molore...</p>
    </div><!-- close SlideInfo -->
  </header>
</div><!-- close head -->

<!-- mobile form -->
<div class="mobileForm" >
    <div id="Intro">
      <h4>Request Info Kit</h4>
      <p>Quostecta quostecta eaqui bla quis dolupta nones vendam nones et as nonsequamus.</p>
    </div>
    <b><p>Im a form</p></b>
    <p>eventualty</p>
</div>

<div id="middle">
<div class="content">

<!-- Follow form section -->
  <main>
    <img class="headline" src="/wp-content/themes/bakerLanding/slices/Success Starts Here Headline.png">

      <p>Quostecta quostecta eaqui bla quis dolupta nones vendam nones et as nonsequamus. Explant quostecta eaqui bla quis vendam nones et as nonsequamus molore. Quostecta quostecta eaqui bla quis dolupta nones vendam nones et as nonsequamus. Explant quostecta eaqui bla quis vendam nones et as nonsequamus molore.</p>
      <h5>5 Reasons to enroll</h5>
        <ol>
          <li>Tuition is affordable, and financial aid is available to those who qualify.</li>
          <li>Baker is not-for-profit. that means we invest in YOU, not share holders.</li>
          <li>Lifetime Employment Assistance menas we're here to help you suceed...for free and forever.</li>
          <li>Baker College offers career education and traning on-campus, online or both.</li>
          <li>A top Millitary Friendly college and ranked a top 20 Best for Vets online/nontraditional school</li>
        </ol>
      <h2>Earn your degree</h2>
      <div class="hold">
        <img class="left" src="/wp-content/themes/bakerLanding/slices/AssociatesDegree_photo-1447078806655-40579c2520d6.png" />
        <h3>Associate Degree</h3>
        <p>Eaqui bla quis dolupta nones vendam nones et as nonsequamus eaqui bla quis vendam nones et as.</p>
      </div>

      <div class="hold">
        <img class="right" src="/wp-content/themes/bakerLanding/slices/GettyImages-510216329 - NOT PURCHASED.png" />
        <h3>Bachelor Degree</h3>
        <p>Eaqui bla quis dolupta nones vendam nones et as nonsequamus eaqui bla quis vendam nones et as.</p>
      </div>
      <div class="hold">
        <img class="left" src="/wp-content/themes/bakerLanding/slices/GettyImages-529400803 - NOT PURCHASED.png" />
        <h3>Master Degree</h3>
        <p>Eaqui bla quis dolupta nones vendam nones et as nonsequamus eaqui bla quis vendam nones et as.</p>
      </div>
      <div class="hold">
        <img class="right" src="/wp-content/themes/bakerLanding/slices/GettyImages-510216329 - NOT PURCHASED.png" />
        <h3>Doctoral Degree</h3>
        <p>Eaqui bla quis dolupta nones vendam nones et as nonsequamus eaqui bla quis vendam nones et as.</p>
      </div>
  </main>

  <!-- Follow form -->
  <div class="aside fixme" id="fix" >
      <div id="Intro">
        <h4>Request Info Kit</h4>
        <p>Quostecta quostecta eaqui bla quis dolupta nones vendam nones et as nonsequamus.</p>
      </div>
      <b><p>Im a form</p></b>
      <p>eventualty</p>
  </div>
</div><!-- .content close -->
</div>
<!-- Photo/Instagram tile area-->
<div class="tiles">
  <div id="section">
    <div class="contain">
      <img src="/wp-content/themes/bakerLanding/slices/Find Your People Headline.png">
      <p>Join the housands of Bakrt studentsand Alumni that are enjoing their well deserved sucess. Being proudly passionate anout something can make yoy feel like and anomoly but that is a community of people who are just as dedicated as you are. The meaningful relationships you make here will go far beyond graduation.</p>
      <h6>#IAMBAKER</h6>
    </div>
  </div>
</div>
<!-- CTA -->
<div class="footerSplash">
  <footer>
    <div class="contain">
      <img src="/wp-content/themes/bakerLanding/slices/Get Started Headline.png">
      <a class="button">Get Started</a>
  </footer>
</div>
<?php get_footer(); ?>
