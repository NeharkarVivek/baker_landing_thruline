<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- test for page php -->

<div class="head" style="background-image:url(<?php the_field('big_slide_image');?>);">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="<?php echo get_bloginfo('template_directory');?>/slices/Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    
    <!--<div id="phone">
      <p><span id="">Toll-Free:</span> <a href="tel:855-487-7888">855-487-7888</a></p>
      <a href="tel:855-487-7888" class="phone_icon"></a>
    </div> -->

    <!-- close phone-->


    <!-- Change mobile number only For HS page -->
    <?php if(get_the_title() == 'HS'): ?>
      <div id="phone">
          <p><span id="">Toll-Free:</span> <a href="tel:1-877-351-3771">1-877-351-3771</a></p>
          <a href="tel:1-877-351-3771" class="phone_icon"></a>
        </div><!-- close phone-->
    <?php endif; ?> 

    <!-- Keep Unique mobile number for all other pages --> 
    <?php if(get_the_title() != 'HS'): ?>
      <div id="phone">
          <p><span id="">Toll-Free:</span> <a href="tel:855-487-7888">855-487-7888</a></p>
          <a href="tel:855-487-7888" class="phone_icon"></a>
        </div><!-- close phone-->
    <?php endif; ?>    

    <div id="SlideInfo">

	<div class="VertAlign">
      	<h1><?php the_field('headline'); ?></h1>
		
		<?php if(get_field('play_button_url_')): ?>
			<a href="<?php the_field('play_button_url_'); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/slices/Video Ply BTN.png" alt="Play Video"></a>
		<?php endif; ?>

	<p><?php the_field('sub-headline') ?></p>
	</div>

    </div><!-- close SlideInfo -->

  </header>
</div><!-- close head -->


<?php if(get_the_title() != 'Thank You'): ?>

<section class="content_upper">
  <div class="content">
    <div class="form_holder fixme" id="top">
        <div id="Intro" class="intro">
            <h4><?php the_field('form_title'); ?></h4>
            <p><?php the_field('form_content'); ?></p>
        </div>
      <?php get_template_part('keypathForm'); ?>
    </div>
    <div class="success_holder">
        <?php if(get_field('success_starts_here')): ?>
        <img class="headline" src="<?php the_field('success_starts_here');?>">
        <?php endif; ?>
        <?php the_field('sucess_starts_here_para') ?>
        
    </div>
  </div>
  
</section>

<!-- mobile form -->

<div class="goToFormMobile">
   <a href="#top"><div id="goToFormMobile">
     <h4><?php the_field('form_title'); ?></h4>
     <p>Click here to request information</p>
   </div></a>
</div>

<div id="middle">
<div class="content">

<!-- Follow form section -->
<main>
      
  <div class="avail_holder">
    <?php if(get_field('program_class_heading')): ?>
      <h2 class="availdegree"><?php the_field('program_class_heading') ?></h2>
        <!--<h2 class="availdegree">Available degrees</h2>-->
    <?php endif; ?>

    <span><?php the_field('avail_degrees') ?></span>
  </div>
   
   <div class="programs_row">
      <div class="hold">
        <h3><?php the_field('1st_program_title'); ?></h3>
        <section><?php the_field('1st_program_information'); ?></section>
      </div>

  <?php if(get_field('2nd_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('2nd_program_title')?> </h3>
      <section><?php the_field('2nd_program_information')?></section>
    </div>
  <?php endif; ?>
  </div>
  
  <div class="programs_row">
  <?php if(get_field('3rd_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('3rd_program_title')?> </h3>
      <section><?php the_field('3rd_program_information')?></section>
    </div>
  <?php endif; ?>
  <?php if(get_field('4th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('4th_program_title')?> </h3>
      <section><?php the_field('4th_program_information')?></section>
    </div>
  <?php endif; ?>
  </div>
  <div class="programs_row">
  <?php if(get_field('5th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('5th_program_title')?> </h3>
      <section><?php the_field('5th_program_information')?></section>
    </div>
  <?php endif; ?>
  <?php if(get_field('6th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('6th_program_title')?> </h3>
      <section><?php the_field('6th_program_information')?></section>
    </div>
  <?php endif; ?>
  </div>
  <div class="programs_row">
  <?php if(get_field('7th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('7th_program_title')?> </h3>
      <section><?php the_field('7th_program_information')?></section>
    </div>
  <?php endif; ?>
  <?php if(get_field('8th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('8th_program_title')?> </h3>
      <section><?php the_field('8th_program_information')?></section>
    </div>
  <?php endif; ?>
  </div>
  <div class="programs_row">
  <?php if(get_field('9th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('9th_program_title')?> </h3>
      <section><?php the_field('9th_program_information')?></section>
    </div>
  <?php endif; ?>
  <?php if(get_field('10th_program_title')): ?>
          <div class="hold">
      <h3><?php the_field('10th_program_title')?> </h3>
      <section><?php the_field('10th_program_information')?></section>
    </div>
  <?php endif; ?>
  </div>

</main>

  <!-- Follow form -->
  
</div><!-- .content close -->
</div>


<?php if(get_field('additional_programs_bg_image')): ?>
<section class="additional_programs_holder" style="background-image:url(<?php the_field('additional_programs_bg_image');?>);">
  <div class="content">
      <div class="title"><span class="additional_text">Additional</span><h3><?php the_field('additional_programs_title'); ?></h3></div> 
      <div class="addi_programs"><?php the_field('additional_programs'); ?>    </div>
      <div class="fillout_text">Fill out the form on this page to request more information about these programs.  </div>
  </div>
</section>
<?php endif; ?>

<?php
$CampusLocation = array(
    "Allen Park" => "4500 Enterprise Drive </br>Allen Park, MI 48101",
    "Auburn Hills" => "1500 University Drive </br>Auburn Hills, MI 48326",
    "Cass City" => "6667 Main Street </br>Cass City, MI 48726",
    "Cadillac" => "9600 E. 13th Street </br>Cadillac, MI 49601 ",
    "Clinton Township" => "34401 S. Gratiot Avenue </br>Clinton Township, MI 48035",
    "Coldwater" => "70 E. Chicago Street, Suite 380 </br>Coldwater, MI 49036",
    "Flint" => "1050 W. Bristol Road </br>Flint, MI 48507",
    "Fremont" => "5479 W. 72nd Street, Suite 102 </br>Fremont, MI 49412",
    "Jackson" => "2800 Springport Road </br>Jackson, MI 49202",
    "Muskegon" => "1903 Marquette Avenue </br>Muskegon, MI 49442",
    "Owosso" => "1309 S. M-52 </br>Owosso, MI 48867",
    "Port Huron" => "3402 Lapeer Road </br>Port Huron, MI 48060"
);
$AdditionalLocation = array(
    "Baker Center for Graduate Studies" => "1116 W. Bristol Road </br>Flint, MI 48507",
    "BAKER ONLINE" => "1116 W. Bristol Road </br>Flint, MI 48507",    
);

// CMS details
/*Allen Park : Allen Park
Auburn Hills : Auburn Hills
Cadillac : Cadillac
Cass City : Cass City
Clinton Township : Clinton Township
Coldwater : Coldwater
Flint : Flint
Fremont : Fremont
Jackson : Jackson
Muskegon : Muskegon
Owosso : Owosso
Port Huron : Port Huron*/

?>


<?php if(get_field('locations_bg_image')): ?>

<section class="locations_holder" style="background-image:url(<?php the_field('locations_bg_image');?>);">
  <div class="content">
      <div class="title"><span class="location_text">Locations</span><h3>Michigan</h3></div> 
      <div class="locations_list">
        <?php
          $values = get_field('locations');

          if($values)
          {
            echo '<ul>';

        foreach($values as $value)
        {
          echo '<li><h4>' . $value . '</h4><div>'. $CampusLocation[$value] . '</div></li>';
        }

        echo '</ul>';
      }
        ?>


    


      </div>
      <div class="additional_locations">
    <?php
          $values = get_field('additional_locations');

          if($values)
          {
            echo '<ul>';

        foreach($values as $value)
        {
          echo '<li><h4>' . $value . '</h4><div>'. $AdditionalLocation[$value] . '</div></li>';
        }

        echo '</ul>';
      }
        ?>
      </div>
  </div>
</section>
<?php endif; ?>


<div class="footerSplash" style="background-image:url(<?php the_field('footer_splash');?>);">
  <footer>
    <div class="title">
      <span class="get_text">Get</span>
      <h3>Started Today</h3>
      <a href="<?php the_field('cal_to_action_link') ?>" class="button">GET STARTED</a>
    </div>
    
  </footer>
</div>

<div class="copyright">
<p style="text-align:center;">
 <a href="https://baker.edu/about/policies-procedures/#privacystatement">Privacy Policy</a>  ||  <a href="https://www.baker.edu">Baker Home Page</a>
</p>
</div>

<?php endif; ?> 

<?php endwhile; endif; ?>
<?php get_footer(); ?>
