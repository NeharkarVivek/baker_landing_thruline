<?php
  /*global $base_url;
  $url = $base_url . $_SERVER["REQUEST_URI"];
  $currentPage = $base_url . $_SERVER["REQUEST_URI"];*/
  if ( ! function_exists( 'get_current_page_url' ) ) {
  function get_current_page_url() {
    global $wp;
    return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
  }
  }
  /*
  * Shorthand for echo get_current_page_url();
  * @returns echo'd string
  */
  if ( ! function_exists( 'the_current_page_url' ) ) {
  function the_current_page_url() {
    echo get_current_page_url();
  }
  }

  if( ! is_page('grad-studies-online') ) {
    $formID = "7459";
  }
  if( is_page('grad-studies-online') ) {
    $formID = "7908";
  }

?>

<form name="ppcForm" method=get action="https://webservices.keypathpartners.com/ilm/default.ashx" class="contact-form__form keypath">
  <div class="contact-form__error-message">Errors highlighted in red</div>
  <div class="contact-form__required-text">All Fields Required</div>

  <div class="contact-form__row contact-form__row--two">
    <div class="contact-form__field-wrap">
      <input class="text" type='text' value="" name='firstname' id='first-name' placeholder="First Name" required>
    </div>

    <div class="contact-form__field-wrap">
      <input class="text" type='text' name='lastname' value="" id='last-name' placeholder="Last Name" required>
    </div>
  </div>

  <div class="contact-form__row contact-form__row--two">
    <div class="contact-form__field-wrap">
      <input name="email" type="email" value="" name='email' id='email' placeholder="Email" required>
    </div>

    <div class="contact-form__field-wrap">
      <input name="dayphone" type="tel" id='dayphone' value="" placeholder="Phone" data-inputmask="'mask': '999-999-9999'" required>
    </div>
  </div>

  <div class="contact-form__row">
    <div class="contact-form__field-wrap">
      <input class="text" type='text' name='address' id='address' placeholder="Address" required>
    </div>
  </div>
  <div class="contact-form__row">
    <div class="contact-form__field-wrap">
      <input name="city" type="text" id='city' placeholder="City" required>
    </div>
  </div>

  <div class="contact-form__row contact-form__row--two">
    <div class="contact-form__field-wrap">
      <select name="state" class="js-select2" style="width: 100%" name="state" id="" data-placeholder="State" required>
      <option value=""></option>
      <option class="dropdownvalue" value="AL">Alabama</option>
      <option class="dropdownvalue" value="AK">Alaska</option>
      <option class="dropdownvalue" value="AZ">Arizona</option>
      <option class="dropdownvalue" value="AR">Arkansas</option>
      <option class="dropdownvalue" value="CA">California</option>
      <option class="dropdownvalue" value="CO">Colorado</option>
      <option class="dropdownvalue" value="CT">Connecticut</option>
      <option class="dropdownvalue" value="DE">Delaware</option>
      <option class="dropdownvalue" value="DC">Dist. of Col.</option>
      <option class="dropdownvalue" value="FL">Florida</option>
      <option class="dropdownvalue" value="GA">Georgia</option>
      <option class="dropdownvalue" value="HI">Hawaii</option>
      <option class="dropdownvalue" value="ID">Idaho</option>
      <option class="dropdownvalue" value="IL">Illinois</option>
      <option class="dropdownvalue" value="IN">Indiana</option>
      <option class="dropdownvalue" value="IA">Iowa</option>
      <option class="dropdownvalue" value="KS">Kansas</option>
      <option class="dropdownvalue" value="KY">Kentucky</option>
      <option class="dropdownvalue" value="LA">Louisiana</option>
      <option class="dropdownvalue" value="ME">Maine</option>
      <option class="dropdownvalue" value="MD">Maryland</option>
      <option class="dropdownvalue" value="MA">Massachusetts</option>
      <option class="dropdownvalue" value="MI">Michigan</option>
      <option class="dropdownvalue" value="MN">Minnesota</option>
      <option class="dropdownvalue" value="MS">Mississippi</option>
      <option class="dropdownvalue" value="MO">Missouri</option>
      <option class="dropdownvalue" value="MT">Montana</option>
      <option class="dropdownvalue" value="NE">Nebraska</option>
      <option class="dropdownvalue" value="NV">Nevada</option>
      <option class="dropdownvalue" value="NH">New Hampshire</option>
      <option class="dropdownvalue" value="NJ">New Jersey</option>
      <option class="dropdownvalue" value="NM">New Mexico</option>
      <option class="dropdownvalue" value="NY">New York</option>
      <option class="dropdownvalue" value="NC">North Carolina</option>
      <option class="dropdownvalue" value="ND">North Dakota</option>
      <option class="dropdownvalue" value="OH">Ohio</option>
      <option class="dropdownvalue" value="OK">Oklahoma</option>
      <option class="dropdownvalue" value="OR">Oregon</option>
      <option class="dropdownvalue" value="PA">Pennsylvania</option>
      <option class="dropdownvalue" value="RI">Rhode Island</option>
      <option class="dropdownvalue" value="SC">South Carolina</option>
      <option class="dropdownvalue" value="SD">South Dakota</option>
      <option class="dropdownvalue" value="TN">Tennessee</option>
      <option class="dropdownvalue" value="TX">Texas</option>
      <option class="dropdownvalue" value="UT">Utah</option>
      <option class="dropdownvalue" value="VT">Vermont</option>
      <option class="dropdownvalue" value="VA">Virginia</option>
      <option class="dropdownvalue" value="WA">Washington</option>
      <option class="dropdownvalue" value="WV">West Virginia</option>
      <option class="dropdownvalue" value="WI">Wisconsin</option>
      <option class="dropdownvalue" value="WY">Wyoming</option>
      </select>
    </div>
    <div class="contact-form__field-wrap">
      <input name="zip" type="text" id='zip' placeholder="Zip Code" data-inputmask="'mask': '99999'" required>
    </div>
    <!-- <div class="contact-form__field-wrap">
<label for="country" class="contact-form__label">* Country</label>
<select name="country" class="js-select2" style="width: 100%" name="state" id="" data-placeholder="Select a Country" required>
<option value=""></option>
<option class="dropdownvalue" value="U.S.A.">U.S.A.</option>
<option class="dropdownvalue" value="Canada">Canada</option>
<option class="dropdownvalue" value="Outside U.S.A.">Outside U.S.A.</option>
</select>
</div> -->
  </div>
  <div class="contact-form__row contact-form__row--two">
    <div class="contact-form__field-wrap">
      <label for="gradyear" class="contact-form__label">* High School Graduation Year:</label>
      <select class="js-select2" style="width: 100%" name="gradyear" id="" data-placeholder="Graduation Year" required>
      <option value=""></option>
      <option class="dropdownvalue" value="2020">2020</option>
      <option class="dropdownvalue" value="2019">2019</option>
      <option class="dropdownvalue" value="2018">2018</option>
      <option class="dropdownvalue" value="2017">2017</option>
      <option class="dropdownvalue" value="2016">2016</option>
      <option class="dropdownvalue" value="2015">2015</option>
      <option class="dropdownvalue" value="2014">2014</option>
      <option class="dropdownvalue" value="2013">2013</option>
      <option class="dropdownvalue" value="2012">2012</option>
      <option class="dropdownvalue" value="2011">2011</option>
      <option class="dropdownvalue" value="2010">2010</option>
      <option class="dropdownvalue" value="2009">2009</option>
      <option class="dropdownvalue" value="2008">2008</option>
      <option class="dropdownvalue" value="2007">2007</option>
      <option class="dropdownvalue" value="2006">2006</option>
      <option class="dropdownvalue" value="2005">2005</option>
      <option class="dropdownvalue" value="2004">2004</option>
      <option class="dropdownvalue" value="2003">2003</option>
      <option class="dropdownvalue" value="2002">2002</option>
      <option class="dropdownvalue" value="2001">2001</option>
      <option class="dropdownvalue" value="2000">2000</option>
      <option class="dropdownvalue" value="1999">1999</option>
      <option class="dropdownvalue" value="1998">1998</option>
      <option class="dropdownvalue" value="1997">1997</option>
      <option class="dropdownvalue" value="1996">1996</option>
      <option class="dropdownvalue" value="1995">1995</option>
      <option class="dropdownvalue" value="1994">1994</option>
      <option class="dropdownvalue" value="1993">1993</option>
      <option class="dropdownvalue" value="1992">1992</option>
      <option class="dropdownvalue" value="1991">1991</option>
      <option class="dropdownvalue" value="1990">1990</option>
      <option class="dropdownvalue" value="1989">1989</option>
      <option class="dropdownvalue" value="1988">1988</option>
      <option class="dropdownvalue" value="1987">1987</option>
      <option class="dropdownvalue" value="1986">1986</option>
      <option class="dropdownvalue" value="1985">1985</option>
      <option class="dropdownvalue" value="1984">1984</option>
      <option class="dropdownvalue" value="1983">1983</option>
      <option class="dropdownvalue" value="1982">1982</option>
      <option class="dropdownvalue" value="1981">1981</option>
      <option class="dropdownvalue" value="1980">1980</option>
      <option class="dropdownvalue" value="1979">1979</option>
      <option class="dropdownvalue" value="1978">1978</option>
      <option class="dropdownvalue" value="1977">1977</option>
      <option class="dropdownvalue" value="1976">1976</option>
      <option class="dropdownvalue" value="1975">1975</option>
      <option class="dropdownvalue" value="1974">1974</option>
      <option class="dropdownvalue" value="1973">1973</option>
      <option class="dropdownvalue" value="1972">1972</option>
      <option class="dropdownvalue" value="1971">1971</option>
      <option class="dropdownvalue" value="1970">1970</option>
      <option class="dropdownvalue" value="1969">1969</option>
      <option class="dropdownvalue" value="1968">1968</option>
      <option class="dropdownvalue" value="1967">1967</option>
      <option class="dropdownvalue" value="1966">1966</option>
      <option class="dropdownvalue" value="1965">1965</option>
      <option class="dropdownvalue" value="1964">1964</option>
      <option class="dropdownvalue" value="1963">1963</option>
      <option class="dropdownvalue" value="1962">1962</option>
      <option class="dropdownvalue" value="1961">1961</option>
      <option class="dropdownvalue" value="1960">1960</option>
      <option class="dropdownvalue" value="1959">1959</option>
      <option class="dropdownvalue" value="1958">1958</option>
      <option class="dropdownvalue" value="1957">1957</option>
      <option class="dropdownvalue" value="1956">1956</option>
      <option class="dropdownvalue" value="1955">1955</option>
      </select>
    </div>
    <div class="contact-form__field-wrap">
      <label for="LocationID2" class="contact-form__label">*  Desired Campus Location</label>
      <select class="js-select20ff" style="width: 100%" name="LocationID2" id="LocationID2" data-placeholder="Choose a Location" required>
<option value=""></option>
</select>
    </div>
  </div>
  <div class="contact-form__row">
    <div class="contact-form__field-wrap">
      <label for="areaofstudy" class="contact-form__label">* Area of Interest</label>
      <select name='areaofstudy' id="areaofstudy">
      <option value="">Area of Interest:</option>
      </select>
    </div>
  </div>
  <div class="contact-form__row">
    <div class="contact-form__field-wrap">
      <label for="CurriculumID" class="contact-form__label">* Desired Program</label>
      <select class="js-select2Off" style="width: 100%" name="CurriculumID" id="CurriculumID" data-placeholder="Select a Program" required>
<option value=""></option>
</select>
    </div>
  </div>
  <div class="hideme" style="display: none !important;">
	<input type="hidden" name="gacid" id="gacid" value='<?php echo preg_replace("/^.+.(.+?..+?)$/", "\1", @$_COOKIE["_ga"]); ?>'> 
	<input type="hidden" name="msclkid" id="msclkid" value='<?php echo preg_replace("/_uet/", "", @$_COOKIE["_uetmsclkid"]); ?>'>
          <input type='hidden' placeholder="" name='AffiliateLocationID' id='AffiliateLocationID' value=0 >
          <input type='hidden' placeholder="" name='FormID' id='FormID' value="<?php echo $formID; ?>" >
          <!--<input type='hidden' placeholder="" name='FormID' id='FormID' value=7459 >
          <input type='hidden' placeholder="" name='FormID' id='FormID' value=7908 >-->
          <input type='hidden' placeholder="" name='LocationID' id='LocationID' value=1286 >

          <input type='hidden' placeholder="" name='CampaignID' id='CampaignID' value=7172 >
          <input type='hidden' placeholder="" name='VendorID' id='VendorID' value=38999 >

      <!--<input type='hidden' placeholder="" name='CaptureURL' id='CaptureURL' value='<?php echo get_current_page_url(); ?>'>-->
      <input type='hidden' placeholder="" name='ReturnToURL' id="ReturnToURL" value="https://discover.baker.edu/here-for-the-making/thank-you/">

      <input type="hidden" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>">
      <input type="hidden" name="SearchEngine" id="SearchEngine" value="<?php echo $_REQUEST['SearchEngine']?>">
      <input type="hidden" name="SearchEngineCampaign" id="SearchEngineCampaign" value="<?php echo $_REQUEST['SearchEngine']?>">
      <input type="hidden" name="VendorAccountID" id="VendorAccountID" value="<?php echo $_REQUEST['VendorAccountID']?>">

      <input type='hidden' placeholder="" name='ClientSourceCode' id='ClientSourceCode' value="<?php echo $_REQUEST['ClientSourceCode']?>" >
  </div>

  <input name="" type="submit" class="btn-cta-round btn-cta-round--red contact-form__submit" value="Submit">

  <script src="//webservices.plattformad.com/cfe/JSON.ashx?JSONAction=FormProgramsCurriculumGroup&formid=<?php echo $formID; ?>&CurriculumCategoryID=8" type="text/javascript"></script>
  <!--<script src="//webservices.plattformad.com/cfe/scripts/JSONObjectCurriculumHandlerCID.js" type="text/javascript"></script>-->
  <script src="//tracking.plattformad.com/" type="text/javascript"></script>


  <script src="https://webservices.plattformad.com/cfe/scripts/JSONObjectCurriculumHandlerCID.js" type="text/javascript"></script>

  <script>

  $.validator.addMethod('customphone', function (value, element) {
    return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
  }, "Please enter a valid phone number");

  $.validator.methods.email = function( value, element ) {
  return this.optional( element ) || /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/.test( value );
  }

  $.validator.addMethod("fillProgram", function(value, element, param) {
    return this.optional(element) || value != param;
  }, "Please specify the program you are interested in");

  $.validator.addMethod("zipValid", function(value, element) {
    return this.optional(element) || /^\d{5}$/.test(value);
  }, "Please provide a valid zipcode.");

  $.validator.addMethod("nonNumeric", function(value, element) {
    return this.optional(element) || !value.match(/[0-9]+/);
  },"Only alphabatic characters allowed.");

  Inputmask.extendDefaults({
    'autoUnmask': true
  });

  $().ready(function() {
    // validate signup form on keyup and submit
    $(".keypath").validate({
      rules: {
        firstname: "required",
        lastname: "required",
        email: {
          required: true,
        },
        dayphone: "customphone",
        address: "required",
        city: {
          required: true,
          nonNumeric: true
        },
        state: "required",
        zip: "zipValid",
        gradyear: "required",
        LocationID: "required",
        CurriculumID: "required",
      },
      messages: {
        firstname: "Please enter your First Name",
        lastname: "Please enter your Last Name",
        email: "Please enter a valid email address",
        dayphone: "Please enter a valid phone number",
        address: "Please enter your Address",
        city: "Please enter your City",
        state: "Please enter your State",
        zip: "Please enter your Zip Code",
        gradyear: "Please enter your Graduation Year",
        LocationID: "Please enter your Preferred campus location",
        CurriculumID: "Please specify the program you are interested in",
      }
    });
  });

  </script>


  <script type="text/javascript">
          /*var __ProgramPleaseSelectText = "Select a program";
          var __LocationPleaseSelectText = "Location of interest";

          InitCurriculumDropDown('LocationID2','CurriculumID');

          var x = document.getElementById("LocationID2");
          if (x.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
              x.removeEventListener("change", FillPrograms);
          } else if (x.detachEvent) {                    // For IE 8 and earlier versions
              x.detachEvent("change", FillPrograms);
          }

          document.getElementById("LocationID2").onchange = function(){
              FillPrograms(1286);
          };*/

          var __locationObj;
          var __programObj;
          var __ProgramOptionPleaseSelectText = "Please select area of interest";
          var __ProgramOptionPleaseSelectFirstText = "Please select a campus first";
          var __ProgramPleaseSelectText = "Please select a program";
          var __ProgramPleaseSelectLocationFirstText = "Please select area of interest first";
          var __LocationPleaseSelectText = "Please select campus";
          var __ProgramOptionGroup = null;
          var __CurriculumObject;




      InitCurriculumDropDown('LocationID2','CurriculumID', 'areaofstudy');

      document.getElementById("LocationID2").onchange = function(){

          if ( this.value != "" && this.value.length != 0 ) {  FillOptionGroups(1286);  }

      };
      document.getElementById("areaofstudy").onchange = function(){

          if ( this.value != "" && this.value.length != 0 ) {  FillPrograms(1286);   }

      };
          /*var x = document.getElementById("LocationID2");
          if (x.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
              x.removeEventListener("change", FillPrograms);
          } else if (x.detachEvent) {                    // For IE 8 and earlier versions
              x.detachEvent("change", FillPrograms);
          }*/



          function ObjectSize (o) { var len = 0; for (var k in o) len++; return len;}
          function ObjectGetFirst (o) { var len = 0; for (var k in o) return k;}

          function InitCurriculumDropDown(LocationElementID, ProgramElementID, ProgramOptionGroup)
          {
              __CurriculumObject = _LocationID;
              __locationObj = document.getElementById(LocationElementID);
              __programObj = document.getElementById(ProgramElementID);

              if( ObjectSize(__CurriculumObject) > 1)
              {
                  ClearDropDown(__locationObj);
                  AddOption(__locationObj,__LocationPleaseSelectText);
                  for(var c in __CurriculumObject)
                  {
                      AddOption(__locationObj, __CurriculumObject[c].DisplayValue, c);
                  }
              }
              else
              {
                  __locationObj.value = ObjectGetFirst(__CurriculumObject);
                  FillPrograms(LocationElementID, ProgramElementID);
              }

              if(ProgramOptionGroup!=null)
              {
                  __ProgramOptionGroup = document.getElementById(ProgramOptionGroup);
                  XBrowserAddHandler(__locationObj,"change",FillOptionGroups);
                  XBrowserAddHandler(__ProgramOptionGroup,"change",FillPrograms);
                  FillOptionGroups();
              }
              else
              {
                  XBrowserAddHandler(__locationObj,"change",FillPrograms);
                  FillPrograms();
              }
          }

          function ClearDropDown(obj)
          {
              // Clear Options and groups
              while (obj.firstChild) {obj.removeChild(obj.firstChild);}
          }

          function AddOption(o,t,v)
          {
              var Opt = document.createElement('option');
              Opt.text = t;
              if (v == undefined || v == "")
                  Opt.value = "";
              else
                  Opt.value = v;
              try {o.add(Opt, null); }
              catch(ex) {o.add(Opt); }// IE only
          }


          function FillOptionGroups(CampusID)
          {
              if(isNaN(CampusID)) CampusID = __locationObj.value;
              ClearDropDown(__ProgramOptionGroup);

              if(CampusID > 0 && __CurriculumObject[CampusID] != null)
              {
                  oGroups = __CurriculumObject[CampusID].Curriculum;
                  AddOption(__ProgramOptionGroup, __ProgramOptionPleaseSelectText);
                  for(g in oGroups)
                  {
                      if(!document.getElementById("__optionDD" + oGroups[g].CurriculumGroupDisplayValue))
                      {
                          var option = new Option(oGroups[g].CurriculumGroupDisplayValue, oGroups[g].CurriculumGroupDisplayValue)
                          option.innerText = oGroups[g].CurriculumGroupDisplayValue;
                          if(oGroups[g].CurriculumGroupDisplayValue == "Health Services"){
                              option.innerText = "Health Science";
                          }
                          if(oGroups[g].CurriculumGroupDisplayValue == "General Studies"){
                              option.innerText = "Interdisciplinary Studies";
                          }
                          option.id = "__optionDD" + oGroups[g].CurriculumGroupDisplayValue;

                          __ProgramOptionGroup.appendChild(option);
                      }
                  }
              }
              else
                  AddOption(__ProgramOptionGroup, __ProgramOptionPleaseSelectFirstText);

              FillPrograms(CampusID);
          }

          function FillPrograms(CampusID, ProgramElementID)
          {
              if(ProgramElementID!=null) __programObj = document.getElementById(ProgramElementID);
              if(isNaN(CampusID)) CampusID = __locationObj.value;

              ClearDropDown(__programObj);

              if(__ProgramOptionGroup != null && __ProgramOptionGroup.selectedIndex < 1)
              {
                  AddOption(__programObj, __ProgramPleaseSelectLocationFirstText);
                  return;
              }

              if(CampusID > 0 && __CurriculumObject[CampusID] != null)
              {
                  AddOption(__programObj, __ProgramPleaseSelectText);
                  oProgs = __CurriculumObject[CampusID].Curriculum;
                  var NotSure = false;
                  for(p in oProgs)
                  {
                      if(oProgs[p].ProgramName != 'Not Sure')
                      {
                          var option = new Option(oProgs[p].CurriculumDisplayValue, oProgs[p].CurriculumID)
                          option.innerText = oProgs[p].CurriculumDisplayValue;

                          if(oProgs[p].OptionGroupDisplayValue != '' && __ProgramOptionGroup == null)
                          {
                              // Check if option group already exists
                              if(document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue) == null)
                              {
                                  // create option group
                                  var oGroup = document.createElement('optgroup');
                                  oGroup.id    = 'optgroup' + oProgs[p].OptionGroupDisplayValue;
                                  oGroup.label = oProgs[p].OptionGroupDisplayValue;
                                  __programObj.appendChild(oGroup);
                              }
                              document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue).appendChild(option);
                          }
                          else
                          {
                              if(__ProgramOptionGroup!=null)
                              {
                                  if(__ProgramOptionGroup.value == oProgs[p].CurriculumGroupDisplayValue){
                                      //__programObj.appendChild(option);


                                          if(document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue) == null)
                                          {
                                              // create option group
                                              var oGroup = document.createElement('optgroup');
                                              oGroup.id    = 'optgroup' + oProgs[p].OptionGroupDisplayValue;
                                              oGroup.label = oProgs[p].OptionGroupDisplayValue;
                                              __programObj.appendChild(oGroup);
                                          }
                                          document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue).appendChild(option);


                                  }
                              }
                              else
                                  __programObj.appendChild(option);
                          }
                      }
                      else
                      {
                          NotSure = true;
                      }
                  }
                  if(NotSure)
                  {
                      var option = new Option('Not Sure', 'Not Sure');
                      option.innerText = 'Not Sure';
                      oProg.appendChild(option);
                  }
              }
              else
              {
                  AddOption(__programObj, __ProgramPleaseSelectLocationFirstText);
              }
          }

          function XBrowserAddHandler(target,eventName,handlerName)
          {
              if ( target.addEventListener )
                  target.addEventListener(eventName, handlerName, false);
              else if ( target.attachEvent )
                  target.attachEvent("on" + eventName, handlerName);
              else
                  target["on" + eventName] = handlerName;
          }


  </script>


  <script src="//tracking.plattformad.com/LeadSourceRuntime.aspx?&amp;&amp;REFERER_SITE=<?php echo get_site_url(); ?>" language="javascript"></script>
  <script src="//artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js" type="text/javascript"></script>

</form>
