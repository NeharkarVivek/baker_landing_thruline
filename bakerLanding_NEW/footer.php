<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after
 */
?>
<?php //the_field('extratracking_code'); ?>
<?php if(get_the_title() == 'Thank You'): ?>
    <script type="text/javascript">
                window._vis_opt_queue = window._vis_opt_queue || [];
                window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(200);});
    </script>    
<?php endif; ?> 
<?php if(get_the_title() != 'Thank You'): ?>    
<?php endif; ?> 
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/jquery.validate.js"></script>
  <script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/additional-methods.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.phone.extensions.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/js/jquery.inputmask.js"></script>-->
<?php wp_footer(); ?>
</body>
</html>