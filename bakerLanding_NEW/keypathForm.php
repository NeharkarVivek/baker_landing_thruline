<?php  
  /*global $base_url;  
  $url = $base_url . $_SERVER["REQUEST_URI"];
  $currentPage = $base_url . $_SERVER["REQUEST_URI"];*/
  if ( ! function_exists( 'get_current_page_url' ) ) {
  function get_current_page_url() {
    global $wp;
    return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
  }
  }
  /*
  * Shorthand for echo get_current_page_url(); 
  * @returns echo'd string
  */
  if ( ! function_exists( 'the_current_page_url' ) ) {
  function the_current_page_url() {
    echo get_current_page_url();
  }
  }

  if( ! is_page('grad-studies-online') ) {
    $formID = "7459";
  }
  if( is_page('grad-studies-online') ) {
    $formID = "7908";
  }
  
?>



<form class="keypath" action="//webservices.plattformpartners.com/ilm/default.ashx" id="enterpriseform"  method="post" name="enterpriseform" novalidate="novalidate">
<div class="step current">
        <select name="LocationID2" id="LocationID2" class="dropdowntext">
            <option value="" selected="selected">Select a Location</option>
            <!--<option class="dropdownvalue" selected disabled>Location of interest</option>
            <option class="dropdownvalue" value="42975">Baker College - Allen Park</option>
            <option class="dropdownvalue" value="42977">Baker College - Auburn Hills</option>
            <option class="dropdownvalue" value="42978">Baker College - Cadillac</option>
            <option class="dropdownvalue" value="42981">Baker College - Cass City</option>
            <option class="dropdownvalue" value="42982">Baker College - Clinton Township</option>
            <option class="dropdownvalue" value="42983">Baker College - Coldwater</option>
            <option class="dropdownvalue" value="42984">Baker College - Flint</option>
            <option class="dropdownvalue" value="42985">Baker College - Fremont</option>
            <option class="dropdownvalue" value="42986">Baker College - Jackson</option>
            <option class="dropdownvalue" value="42987">Baker College - Muskegon</option>
            <option class="dropdownvalue" value="42988">Baker College - Owosso</option>
            <option class="dropdownvalue" value="42989">Baker College - Port Huron</option>
            <option class="dropdownvalue" value="42990">Baker College - Reading</option>
            <option class="dropdownvalue" value="1286">Baker College Online</option>
            <option class="dropdownvalue" value="43095">Baker Center for Graduate Studies (Online)</option>-->
        </select>
<select name='areaofstudy' id="areaofstudy">
<option value="">Area of Interest:</option>
</select>

<select name='CurriculumID' id="CurriculumID">
<option value="">Program of Interest:</option>
</select>
<div style="text-align: center; margin-top: 12px;" class="two-step-form-customization display-in-normal-form"><input type='submit' name='Submit' value="Send Request"></div>
<div style="text-align: center; margin-top: 12px;" class="two-step-form-customization"><button class="nextstep button">Next Step</button></div>
 <span class="stepinfo step1of2 two-step-form-customization">step 1 of 2</span>
</div>
<div class='step step2'>
<div class="full">
 <span class="left"><input type='text' name='firstname' id='firstname'  placeholder="First Name" length=30></span>
 <span class="right"><input type='text' name='lastname' id='lastname' placeholder="Last Name" length=30></span>
</div>
<input type='text' name='email' id='email' placeholder="Email" length=30>
<input type="tel" name='dayphone' id='dayphone' placeholder="Phone" class="required" data-inputmask="'mask': '999-999-9999'" length=30>
<!--<input type='text' name='address' id='address' placeholder="Address" length=30>-->
<input type='tel' name='zip' id='zip' placeholder="Zip Code" data-inputmask="'mask': '99999'" length=30>
<div class="full city_state_wrapper">
 <span class="left"><input type='text' name='city_temp' id='city_temp' placeholder="City" length=30></span>
 <span class="right"> 
    <select name="state_temp" id="state_temp">
    <option selected disabled>State</option>
    <option value="AL">Alabama</option>
    <option value="AK">Alaska</option>
    <option value="AZ">Arizona</option>
    <option value="AR">Arkansas</option>
    <option value="CA">California</option>
    <option value="CO">Colorado</option>
    <option value="CT">Connecticut</option>
    <option value="DE">Delaware</option>
    <option value="DC">Dist. of Col.</option>
    <option value="FL">Florida</option>
    <option value="GA">Georgia</option>
    <option value="HI">Hawaii</option>
    <option value="ID">Idaho</option>
    <option value="IL">Illinois</option>
    <option value="IN">Indiana</option>
    <option value="IA">Iowa</option>
    <option value="KS">Kansas</option>
    <option value="KY">Kentucky</option>
    <option value="LA">Louisiana</option>
    <option value="ME">Maine</option>
    <option value="MD">Maryland</option>
    <option value="MA">Massachusetts</option>
    <option value="MI">Michigan</option>
    <option value="MN">Minnesota</option>
    <option value="MS">Mississippi</option>
    <option value="MO">Missouri</option>
    <option value="MT">Montana</option>
    <option value="NE">Nebraska</option>
    <option value="NV">Nevada</option>
    <option value="NH">New Hampshire</option>
    <option value="NJ">New Jersey</option>
    <option value="NM">New Mexico</option>
    <option value="NY">New York</option>
    <option value="NC">North Carolina</option>
    <option value="ND">North Dakota</option>
    <option value="OH">Ohio</option>
    <option value="OK">Oklahoma</option>
    <option value="OR">Oregon</option>
    <option value="PA">Pennsylvania</option>
    <option value="RI">Rhode Island</option>
    <option value="SC">South Carolina</option>
    <option value="SD">South Dakota</option>
    <option value="TN">Tennessee</option>
    <option value="TX">Texas</option>
    <option value="UT">Utah</option>
    <option value="VT">Vermont</option>
    <option value="VA">Virginia</option>
    <option value="WA">Washington</option>
    <option value="WV">West Virginia</option>
    <option value="WI">Wisconsin</option>
    <option value="WY">Wyoming</option>
    </select>
 </span>
</div>
<select placeholder="" name="gradyear" id="gradyear">
<option selected disabled>High School Graduation Year</option>
    <option value="2020">2020</option>
    <option value="2019">2019</option>
    <option value="2018">2018</option>
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    <option value="2015">2015</option>
    <option value="2014">2014</option>
    <option value="2013">2013</option>
    <option value="2012">2012</option>
    <option value="2011">2011</option>
    <option value="2010">2010</option>
    <option value="2009">2009</option>
    <option value="2008">2008</option>
    <option value="2007">2007</option>
    <option value="2006">2006</option>
    <option value="2005">2005</option>
    <option value="2004">2004</option>
    <option value="2003">2003</option>
    <option value="2002">2002</option>
    <option value="2001">2001</option>
    <option value="2000">2000</option>
    <option value="1999">1999</option>
    <option value="1998">1998</option>
    <option value="1997">1997</option>
    <option value="1996">1996</option>
    <option value="1995">1995</option>
    <option value="1994">1994</option>
    <option value="1993">1993</option>
    <option value="1992">1992</option>
    <option value="1991">1991</option>
    <option value="1990">1990</option>
    <option value="1989">1989</option>
    <option value="1988">1988</option>
    <option value="1987">1987</option>
    <option value="1986">1986</option>
    <option value="1985">1985</option>
    <option value="1984">1984</option>
    <option value="1983">1983</option>
    <option value="1982">1982</option>
    <option value="1981">1981</option>
    <option value="1980">1980</option>
    <option value="1979">1979</option>
    <option value="1978">1978</option>
    <option value="1977">1977</option>
    <option value="1976">1976</option>
    <option value="1975">1975</option>
    <option value="1974">1974</option>
    <option value="1973">1973</option>
    <option value="1972">1972</option>
    <option value="1971">1971</option>
    <option value="1970">1970</option>
    <option value="1969">1969</option>
    <option value="1968">1968</option>
    <option value="1967">1967</option>
    <option value="1966">1966</option>
    <option value="1965">1965</option>
    <option value="1964">1964</option>
    <option value="1963">1963</option>
    <option value="1962">1962</option>
    <option value="1961">1961</option>
    <option value="1960">1960</option>
    <option value="1959">1959</option>
    <option value="1958">1958</option>
    <option value="1957">1957</option>
    <option value="1956">1956</option>
    <option value="1955">1955</option>
</select>
<div style="text-align: center; margin-top: 12px;" class="two-step-form-customization"><input type='submit' name='Submit' value="Send Request"></div>
 <span class="stepinfo goback two-step-form-customization">Go Back</span>
</div>

<div class="hideme" style="display: none !important;">

        <input type='hidden' placeholder="" name='AffiliateLocationID' id='AffiliateLocationID' value=0 >
        <input type='hidden' placeholder="" name='FormID' id='FormID' value="<?php echo $formID; ?>" >
        <!--<input type='hidden' placeholder="" name='FormID' id='FormID' value=7459 >
        <input type='hidden' placeholder="" name='FormID' id='FormID' value=7908 >-->
        <input type='hidden' placeholder="" name='LocationID' id='LocationID' value=1286 >
       
        <input type='hidden' placeholder="" name='CampaignID' id='CampaignID' value=7172 >
        <input type='hidden' placeholder="" name='VendorID' id='VendorID' value=38999 >        
        <input type='hidden' placeholder="" name='city' id='city' value="" >
        <input type='hidden' placeholder="" name='state' id='state' value="" >

    <!--<input type='hidden' placeholder="" name='CaptureURL' id='CaptureURL' value='<?php echo get_current_page_url(); ?>'>-->
    <input type='hidden' placeholder="" name='ReturnToURL' id="ReturnToURL" value="<?php echo get_site_url(); ?>/thankyou/">

    <input type="hidden" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>">
    <input type="hidden" name="SearchEngine" id="SearchEngine" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="SearchEngineCampaign" id="SearchEngineCampaign" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="VendorAccountID" id="VendorAccountID" value="<?php echo $_REQUEST['VendorAccountID']?>">

    <input type='hidden' placeholder="" name='ClientSourceCode' id='ClientSourceCode' value="<?php echo $_REQUEST['ClientSourceCode']?>" >
<input type="hidden" name="gacid" id="gacid" value='<?php echo preg_replace("/^.+\.(.+?\..+?)$/", "\\1", @$_COOKIE["_ga"]); ?>'>
<input type="hidden" name="msclkid" id="msclkid" value='<?php echo preg_replace("/_uet/", "", @$_COOKIE["_uetmsclkid"]); ?>'>
</div>

</form>


<script src="//webservices.plattformad.com/cfe/JSON.ashx?JSONAction=FormProgramsCurriculumGroup&formid=<?php echo $formID; ?>&CurriculumCategoryID=8" type="text/javascript"></script>
<!--<script src="//webservices.plattformad.com/cfe/scripts/JSONObjectCurriculumHandlerCID.js" type="text/javascript"></script>-->
<!-- <script src="//tracking.plattformad.com/" type="text/javascript"></script> -->
<script type="text/javascript">
        /*var __ProgramPleaseSelectText = "Select a program";
        var __LocationPleaseSelectText = "Location of interest";

        InitCurriculumDropDown('LocationID2','CurriculumID');

        var x = document.getElementById("LocationID2");
        if (x.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
            x.removeEventListener("change", FillPrograms);
        } else if (x.detachEvent) {                    // For IE 8 and earlier versions
            x.detachEvent("change", FillPrograms);
        }

        document.getElementById("LocationID2").onchange = function(){
            FillPrograms(1286);
        };*/

        var __locationObj;
        var __programObj;
        var __ProgramOptionPleaseSelectText = "Please select area of interest";
        var __ProgramOptionPleaseSelectFirstText = "Please select a campus first";
        var __ProgramPleaseSelectText = "Please select a program";
        var __ProgramPleaseSelectLocationFirstText = "Please select area of interest first";
        var __LocationPleaseSelectText = "Please select campus";
        var __ProgramOptionGroup = null;
        var __CurriculumObject;

 


    InitCurriculumDropDown('LocationID2','CurriculumID', 'areaofstudy');

    document.getElementById("LocationID2").onchange = function(){
        
        if ( this.value != "" && this.value.length != 0 ) {  FillOptionGroups(1286);  }
        
    };
    document.getElementById("areaofstudy").onchange = function(){
        
        if ( this.value != "" && this.value.length != 0 ) {  FillPrograms(1286);   }
        
    };
        /*var x = document.getElementById("LocationID2");
        if (x.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
            x.removeEventListener("change", FillPrograms);
        } else if (x.detachEvent) {                    // For IE 8 and earlier versions
            x.detachEvent("change", FillPrograms);
        }*/

        

        function ObjectSize (o) { var len = 0; for (var k in o) len++; return len;}
        function ObjectGetFirst (o) { var len = 0; for (var k in o) return k;}

        function InitCurriculumDropDown(LocationElementID, ProgramElementID, ProgramOptionGroup)
        {
            __CurriculumObject = _LocationID;
            __locationObj = document.getElementById(LocationElementID);
            __programObj = document.getElementById(ProgramElementID);
            
            if( ObjectSize(__CurriculumObject) > 1)
            {
                ClearDropDown(__locationObj);
                AddOption(__locationObj,__LocationPleaseSelectText);
                for(var c in __CurriculumObject)
                {
                    AddOption(__locationObj, __CurriculumObject[c].DisplayValue, c);
                }
            }
            else
            {
                __locationObj.value = ObjectGetFirst(__CurriculumObject);
                FillPrograms(LocationElementID, ProgramElementID);
            }
                
            if(ProgramOptionGroup!=null)
            {
                __ProgramOptionGroup = document.getElementById(ProgramOptionGroup);
                XBrowserAddHandler(__locationObj,"change",FillOptionGroups);
                XBrowserAddHandler(__ProgramOptionGroup,"change",FillPrograms);
                FillOptionGroups();
            }
            else
            {   
                XBrowserAddHandler(__locationObj,"change",FillPrograms);
                FillPrograms();     
            }
        }

        function ClearDropDown(obj)
        {
            // Clear Options and groups
            while (obj.firstChild) {obj.removeChild(obj.firstChild);}
        }

        function AddOption(o,t,v)
        {
            var Opt = document.createElement('option');
            Opt.text = t;
            if (v == undefined || v == "")
                Opt.value = "";
            else
                Opt.value = v;
            try {o.add(Opt, null); }
            catch(ex) {o.add(Opt); }// IE only
        }


        function FillOptionGroups(CampusID)
        {
            if(isNaN(CampusID)) CampusID = __locationObj.value;
            ClearDropDown(__ProgramOptionGroup);
            
            if(CampusID > 0 && __CurriculumObject[CampusID] != null)
            {
                oGroups = __CurriculumObject[CampusID].Curriculum;
                AddOption(__ProgramOptionGroup, __ProgramOptionPleaseSelectText);
                for(g in oGroups)
                {
                    if(!document.getElementById("__optionDD" + oGroups[g].CurriculumGroupDisplayValue))
                    {
                        var option = new Option(oGroups[g].CurriculumGroupDisplayValue, oGroups[g].CurriculumGroupDisplayValue)
                        option.innerText = oGroups[g].CurriculumGroupDisplayValue;
                        if(oGroups[g].CurriculumGroupDisplayValue == "Health Services"){
                            option.innerText = "Health Science";    
                        }
                        if(oGroups[g].CurriculumGroupDisplayValue == "General Studies"){
                            option.innerText = "Interdisciplinary Studies";    
                        }                
                        option.id = "__optionDD" + oGroups[g].CurriculumGroupDisplayValue;
                    
                        __ProgramOptionGroup.appendChild(option);
                    }
                }
            }
            else
                AddOption(__ProgramOptionGroup, __ProgramOptionPleaseSelectFirstText);
            
            FillPrograms(CampusID);
        }

        function FillPrograms(CampusID, ProgramElementID)
        {
            if(ProgramElementID!=null) __programObj = document.getElementById(ProgramElementID);    
            if(isNaN(CampusID)) CampusID = __locationObj.value;
            
            ClearDropDown(__programObj);    
            
            if(__ProgramOptionGroup != null && __ProgramOptionGroup.selectedIndex < 1)
            {
                AddOption(__programObj, __ProgramPleaseSelectLocationFirstText);
                return;
            }
            
            if(CampusID > 0 && __CurriculumObject[CampusID] != null)
            {
                AddOption(__programObj, __ProgramPleaseSelectText);
                oProgs = __CurriculumObject[CampusID].Curriculum;
                var NotSure = false;
                for(p in oProgs)
                {
                    if(oProgs[p].ProgramName != 'Not Sure')
                    {
                        var option = new Option(oProgs[p].CurriculumDisplayValue, oProgs[p].CurriculumID)
                        option.innerText = oProgs[p].CurriculumDisplayValue;
                        
                        if(oProgs[p].OptionGroupDisplayValue != '' && __ProgramOptionGroup == null)
                        {
                            // Check if option group already exists
                            if(document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue) == null)
                            {
                                // create option group
                                var oGroup = document.createElement('optgroup');
                                oGroup.id    = 'optgroup' + oProgs[p].OptionGroupDisplayValue;      
                                oGroup.label = oProgs[p].OptionGroupDisplayValue;       
                                __programObj.appendChild(oGroup);
                            }           
                            document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue).appendChild(option);
                        }
                        else
                        {
                            if(__ProgramOptionGroup!=null)
                            {
                                if(__ProgramOptionGroup.value == oProgs[p].CurriculumGroupDisplayValue){
                                    //__programObj.appendChild(option);   
                                    
                                    
                                        if(document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue) == null)
                                        {
                                            // create option group
                                            var oGroup = document.createElement('optgroup');
                                            oGroup.id    = 'optgroup' + oProgs[p].OptionGroupDisplayValue;      
                                            oGroup.label = oProgs[p].OptionGroupDisplayValue;       
                                            __programObj.appendChild(oGroup);
                                        }           
                                        document.getElementById('optgroup' + oProgs[p].OptionGroupDisplayValue).appendChild(option);
                                                                        
                                        
                                }
                            }
                            else    
                                __programObj.appendChild(option);
                        }
                    }
                    else
                    {
                        NotSure = true;
                    }
                }
                if(NotSure)
                {
                    var option = new Option('Not Sure', 'Not Sure');
                    option.innerText = 'Not Sure';
                    oProg.appendChild(option);
                }
            }
            else
            {
                AddOption(__programObj, __ProgramPleaseSelectLocationFirstText);
            }
        }

        function XBrowserAddHandler(target,eventName,handlerName)
        {
            if ( target.addEventListener )
                target.addEventListener(eventName, handlerName, false);
            else if ( target.attachEvent )
                target.attachEvent("on" + eventName, handlerName);
            else
                target["on" + eventName] = handlerName;
        }


</script>


<!-- <script src="//tracking.plattformad.com/LeadSourceRuntime.aspx?&amp;&amp;REFERER_SITE=<?php //echo get_site_url(); ?>" language="javascript"></script> -->
<!--<script src="//artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js" type="text/javascript"></script> -->
<script type="text/javascript">
    var v = getUrlVars(document.referrer);
    var q = v.q;
    var p = v.p;

    var ref = document.referrer.split('/');

    try
    {
        if (q)
        {
            if (document.getElementById("SearchKeyword"))
            {
                document.getElementById("SearchKeyword").value = q;
            }
            eraseCookie('SearchKeyword');
            createCookie('SearchKeyword',q,1);
            if (document.getElementById("SearchEngine"))
            {
                document.getElementById("SearchEngine").value = ref[2];
            }
            eraseCookie('SearchEngine');
            createCookie('SearchEngine',ref[2],1);
        }
        else if (p)
        {
            if (document.getElementById("SearchKeyword"))
            {
                document.getElementById("SearchKeyword").value = p;
            }
            eraseCookie('SearchKeyword');
            createCookie('SearchKeyword',p,1);
            if (document.getElementById("SearchEngine"))
            {
                document.getElementById("SearchEngine").value = ref[2];
            }
            eraseCookie('SearchEngine');
            createCookie('SearchEngine',ref[2],1);
        }
        else if(readCookie('SearchKeyword') || readCookie('SearchEngine'))
        {
            document.getElementById("SearchKeyword").value = readCookie('SearchKeyword');
            document.getElementById("SearchEngine").value = readCookie('SearchEngine');
        }
        
        if(!readCookie('SearchEngine') && ref[2] != undefined)
        {
            if (document.getElementById("SearchEngine"))
            {
                document.getElementById("SearchEngine").value = ref[2];
            }
            eraseCookie('SearchEngine');
            createCookie('SearchEngine',ref[2],1);
        }
        
        var d = new Date();
        
        if(!readCookie('NumberOfVisits'))
        {
            //This is the users first visit;
            createCookie('NumberOfVisits',1,365); //storing the value as 1 just to have a value there
        }
        
        if(readCookie('TimeOfLastLoad'))
        {
            if((d.getTime() - parseInt(readCookie('TimeOfLastLoad'))) > 7200000) //hours*60*60*1000
            {
                if(!readCookie('NumberOfVisits'))
                {
                    //This is the users first visit;
                    createCookie('NumberOfVisits',1,365); //storing the value as 1 just to have a value there
                }
                else
                {
                    var visits = parseInt(readCookie('NumberOfVisits')) + 1;
                    eraseCookie('NumberOfVisits');
                    createCookie('NumberOfVisits',visits,365);
                }
            }
        }
        //else we are still in our current session
        
        
        
        if(readCookie('TimeOfLastLoad'))
        {
            //Cookie exists, delete it and create a new one.
            eraseCookie('TimeOfLastLoad');
            createCookie('TimeOfLastLoad',d.getTime(),365);
        }
        else
        {
            createCookie('TimeOfLastLoad',d.getTime(),365);
        }
        
        
        if(!readCookie('OriginalVisit'))
        {
            createCookie('OriginalVisit',d.toUTCString(),365); //Store the users original visit for 365 days
        }
        
        if(!readCookie('LandingPage'))
        {
            createCookie('LandingPage',document.URL,30); //Store the users landing page for 30 days
        }
        
        
        
        
    }
    catch(err)
    {
        
    }

    function getUrlVars(href)
    {
        var vars = [], hash;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    function createCookie(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name,"",-1);
    }
</script>
<script type="text/javascript">
//Inline Script for Tracking leadsource
if(__LeadSourceTracking==null)
{
    var __LeadSourceTracking = true;
    function enc(s){return s.toString().replace(/\%/g, "%26").replace(/=/g,"%3D");}
    function encQString(s){return s.toString().replace("'", "''");}
    document.write('<script language="javascript" src="//tracking.plattformad.com/LeadSourceRuntime.aspx?' + encQString(window.location.search.substring(1)) + '&&REFERER_SITE=' + escape(document.referrer) + '"><\/script>');
}
</script>
<!--<script type="text/javascript" src="//tracking.plattformad.com/"></script>
<script type="text/javascript" src="//artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js"></script>-->



