<?php
/**
 * The template for displaying the header
 */
?>

<!DOCTYPE html>
<head>
   	 <!-- Set the viewport width to device width for mobile -->
   	 <meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Sass Generated File | Sass file /styles.sass
  	<link href="mobile.css" rel="stylesheet" >-->
  	<!-- Sass Generated File | Sass file /styles.sass -->
	<!-- <link href='//fonts.googleapis.com/css?family=Concert One' rel='stylesheet'> -->
  <link href="<?php echo get_bloginfo('template_directory');?>/css/style2.css" rel="stylesheet">
  <link href="<?php echo get_bloginfo('template_directory');?>/css/responsive2.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/animations.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.css">

 	

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/additional-methods.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.phone.extensions.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/js/jquery.inputmask.js"></script>

<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-MFFQLPB':true});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-4538083-1', 'auto');
  ga('require', 'GTM-MFFQLPB');
  ga('send', 'pageview');
</script>


<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KHG9W2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KHG9W2');</script>
<!-- End Google Tag Manager -->

<!-– Oracle Maxymiser Script Start -->
<script type="text/javascript" src="//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!–- Oracle Maxymiser Script End -->
	<?php wp_head(); ?>

</head>

<body class="<?php echo get_the_title();?>" >
