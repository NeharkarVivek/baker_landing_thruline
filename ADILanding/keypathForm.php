<?php  
  /*global $base_url;  
  $url = $base_url . $_SERVER["REQUEST_URI"];
  $currentPage = $base_url . $_SERVER["REQUEST_URI"];*/
  if ( ! function_exists( 'get_current_page_url' ) ) {
  function get_current_page_url() {
    global $wp;
    return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
  }
  }
  /*
  * Shorthand for echo get_current_page_url(); 
  * @returns echo'd string
  */
  if ( ! function_exists( 'the_current_page_url' ) ) {
  function the_current_page_url() {
    echo get_current_page_url();
  }
  }
?>
<form class="keypath" action="//webservices.plattformpartners.com/ilm/default.ashx" id="enterpriseform"  method="post" name="enterpriseform" novalidate="novalidate">
<div class="step step2">
<input type='text' name='firstname' id='firstname'  placeholder="First Name" length=30>
<input type='text' name='lastname' id='lastname' placeholder="Last Name" length=30>
<input type='text' name='email' id='email' placeholder="Email" length=30>
<input type='tel' name='dayphone' id='dayphone' placeholder="Phone" class="required" data-inputmask="'mask': '999-999-9999'" length=30>
<!-- <input type='text' name='address' id='address' placeholder="Address" length=30> -->
<input type='text' name='zip' id='zip' placeholder="Zip Code" data-inputmask="'mask': '99999'" length=30>
<div class="full city_state_wrapper">
  <span class="left"><input type='text' name='city_temp' id='city_temp' placeholder="City" length=30></span>
  <span class="right">
    <select name="state_temp" id="state_temp">
    <option selected disabled>State</option>
    <option value="AL">Alabama</option>
    <option value="AK">Alaska</option>
    <option value="AZ">Arizona</option>
    <option value="AR">Arkansas</option>
    <option value="CA">California</option>
    <option value="CO">Colorado</option>
    <option value="CT">Connecticut</option>
    <option value="DE">Delaware</option>
    <option value="DC">Dist. of Col.</option>
    <option value="FL">Florida</option>
    <option value="GA">Georgia</option>
    <option value="HI">Hawaii</option>
    <option value="ID">Idaho</option>
    <option value="IL">Illinois</option>
    <option value="IN">Indiana</option>
    <option value="IA">Iowa</option>
    <option value="KS">Kansas</option>
    <option value="KY">Kentucky</option>
    <option value="LA">Louisiana</option>
    <option value="ME">Maine</option>
    <option value="MD">Maryland</option>
    <option value="MA">Massachusetts</option>
    <option value="MI">Michigan</option>
    <option value="MN">Minnesota</option>
    <option value="MS">Mississippi</option>
    <option value="MO">Missouri</option>
    <option value="MT">Montana</option>
    <option value="NE">Nebraska</option>
    <option value="NV">Nevada</option>
    <option value="NH">New Hampshire</option>
    <option value="NJ">New Jersey</option>
    <option value="NM">New Mexico</option>
    <option value="NY">New York</option>
    <option value="NC">North Carolina</option>
    <option value="ND">North Dakota</option>
    <option value="OH">Ohio</option>
    <option value="OK">Oklahoma</option>
    <option value="OR">Oregon</option>
    <option value="PA">Pennsylvania</option>
    <option value="RI">Rhode Island</option>
    <option value="SC">South Carolina</option>
    <option value="SD">South Dakota</option>
    <option value="TN">Tennessee</option>
    <option value="TX">Texas</option>
    <option value="UT">Utah</option>
    <option value="VT">Vermont</option>
    <option value="VA">Virginia</option>
    <option value="WA">Washington</option>
    <option value="WV">West Virginia</option>
    <option value="WI">Wisconsin</option>
    <option value="WY">Wyoming</option>
    </select>
  </span>
</div>

<select placeholder="" name="gradyear" id="gradyear">
<option selected disabled>High School Graduation Year</option>
    <option value="2020">2020</option>
    <option value="2019">2019</option>
    <option value="2018">2018</option>
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    <option value="2015">2015</option>
    <option value="2014">2014</option>        
    <option value="2013">2013</option>
    <option value="2012">2012</option>
    <option value="2011">2011</option>
    <option value="2010">2010</option>
    <option value="2009">2009</option>
    <option value="2008">2008</option>
    <option value="2007">2007</option>
    <option value="2006">2006</option>
    <option value="2005">2005</option>
    <option value="2004">2004</option>
    <option value="2003">2003</option>
    <option value="2002">2002</option>
    <option value="2001">2001</option>
    <option value="2000">2000</option>
    <option value="1999">1999</option>
    <option value="1998">1998</option>
    <option value="1997">1997</option>
    <option value="1996">1996</option>
    <option value="1995">1995</option>
    <option value="1994">1994</option>
    <option value="1993">1993</option>
    <option value="1992">1992</option>
    <option value="1991">1991</option>
    <option value="1990">1990</option>
    <option value="1989">1989</option>
    <option value="1988">1988</option>
    <option value="1987">1987</option>
    <option value="1986">1986</option>
    <option value="1985">1985</option>
    <option value="1984">1984</option>
    <option value="1983">1983</option>
    <option value="1982">1982</option>
    <option value="1981">1981</option>
    <option value="1980">1980</option>
    <option value="1979">1979</option>
    <option value="1978">1978</option>
    <option value="1977">1977</option>
    <option value="1976">1976</option>
    <option value="1975">1975</option>
    <option value="1974">1974</option>
    <option value="1973">1973</option>
    <option value="1972">1972</option>
    <option value="1971">1971</option>
    <option value="1970">1970</option>
    <option value="1969">1969</option>
    <option value="1968">1968</option>
    <option value="1967">1967</option>
    <option value="1966">1966</option>
    <option value="1965">1965</option>
    <option value="1964">1964</option>
    <option value="1963">1963</option>
    <option value="1962">1962</option>
    <option value="1961">1961</option>
    <option value="1960">1960</option>
    <option value="1959">1959</option>
    <option value="1958">1958</option>
    <option value="1957">1957</option>
    <option value="1956">1956</option>
    <option value="1955">1955</option>
</select>
<div style="text-align: center; margin-top: 12px;" class="two-step-form-customization"><input type='submit' name='Submit' value="Send Request"></div>
 <span class="stepinfo goback two-step-form-customization">Go Back</span>
</div>

<div class="step current">
<select name='CurriculumID' id="CurriculumID">
<option value="">Program of Interest:</option>
<!--<option value="582289">Automotive Services Technology - Associate</option>
<option value="582288">Automotive Services Technology - Certificate</option>
<option value="582291">Diesel Service Technology - Associate</option>
<option value="582290">Diesel Service Technology - Certificate</option>-->
</select>
<div style="text-align: center; margin-top: 12px;" class="two-step-form-customization display-in-normal-form"><input type='submit' name='Submit' value="Send Request"></div>
<div style="text-align: center; margin-top: 12px;" class="two-step-form-customization"><button class="nextstep button">Next Step</button></div>
 <span class="stepinfo step1of2 two-step-form-customization">step 1 of 2</span>
</div>

<input type='hidden' placeholder="" name='LocationID' id='LocationID' value='42991'>
<input type='hidden' placeholder="" name='AffiliateLocationID' id='AffiliateLocationID' value=0 >
<input type='hidden' placeholder="" name='FormID' id='FormID' value=7452 >
<input type='hidden' placeholder="" name='CampaignID' id='CampaignID' value=7165 >
<input type='hidden' placeholder="" name='VendorID' id='VendorID' value=38999 >        
<input type='hidden' placeholder="" name='city' id='city' value="" >
<input type='hidden' placeholder="" name='state' id='state' value="" >

<div class="hideme" style="display: none !important;">
    
    <input type='hidden' placeholder="" name='CaptureURL' id='CaptureURL' value='<?php echo get_current_page_url(); ?>'>
    <input type='hidden' placeholder="" name='ReturnToURL' id="ReturnToURL" value="<?php echo get_site_url(); ?>/thankyou/">

    <input type="hidden" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>">
    <input type="hidden" name="SearchEngine" id="SearchEngine" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="SearchEngineCampaign" id="SearchEngineCampaign" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="VendorAccountID" id="VendorAccountID" value="<?php echo $_REQUEST['VendorAccountID']?>">

    <input type='hidden' placeholder="" name='ClientSourceCode' id='ClientSourceCode' value="<?php echo $_REQUEST['ClientSourceCode']?>" >
</div>
</form>


<script src="//webservices.plattformad.com/cfe/JSON.ashx?JSONAction=FormProgramsExtended&amp;formid=7452&amp;CurriculumCategoryID=8" type="text/javascript"></script>
<script src="//webservices.plattformad.com/cfe/scripts/JSONObjectCurriculumHandlerCID.js" type="text/javascript"></script>
<script src="//tracking.plattformad.com/" type="text/javascript"></script>
<script type="text/javascript">
var __ProgramPleaseSelectText = "Select a program";
var __LocationPleaseSelectText = "Location of interest";

InitCurriculumDropDown('LocationID','CurriculumID');
</script>
<script src="//tracking.plattformad.com/LeadSourceRuntime.aspx?&amp;&amp;REFERER_SITE=<?php echo get_site_url(); ?>" language="javascript"></script>
<script src="//artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js" type="text/javascript"></script>


<script>
$.validator.addMethod('customphone', function (value, element) {
  /*  return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value); */
  var invalidPhoneNumbers = ["0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999", "1234567890"];
  var valueRegex = value.replace(/[^0-9]/g,'');
  if (value.length > 0 && 10 != valueRegex.length) { return false; }
  return jQuery.inArray(valueRegex, invalidPhoneNumbers) == -1;
}, "Please enter a valid phone number");

$.validator.addMethod("fillProgram", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify the program you are intersted in");

$.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");

$.validator.addMethod("nonNumeric", function(value, element) {
    return this.optional(element) || !value.match(/[0-9]+/);
},"Only alphabatic characters allowed.");

$(document).ready(function() {
  // validate signup form on keyup and submit
  $(".keypath").validate({
    rules: {
      firstname: "required",
      lastname: "required",
      email: {
	required: true, 
	email: true
	},
      dayphone: "customphone",
      address: "required",
      city: {
	required: true,
	nonNumeric: true 
	},
  city_temp: {
    required: true,
    nonNumeric: true
    },
      state: "required",
      state_temp: "required",
      zip: {
	required: true, 
	zipcode: true,
	},
      gradyear: "required",
      LocationID: "required",
      CurriculumID: "required",
    },
    messages: {
      firstname: "Please enter your First Name",
      lastname: "Please enter your Last Name",
      email: "Please enter a valid email address",
      dayphone: "Please enter a valid phone number",
      address: "Please enter your Address",
      city: "Please enter your City",
      state: "Please enter your State",
      city_temp: "Please enter your City",
      state_temp: "Please enter your State",
      zip: "Please enter your Zip Code",
      gradyear: "Please enter your Graduation Year",
      LocationID: "Please enter your Preferred campus location",
      CurriculumID: "Please specify the program you are interested in",
    }
  });
});


$(document).ready(function(){
 $(":input").inputmask();
});

</script>
