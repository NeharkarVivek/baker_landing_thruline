<?php
/**
 * The template for displaying all single posts and attachments
 */
get_header();?>
<!-- Single Post page -->
<div class="head" style="bakcground:<?php get_feild($big_slide_image); ?>;">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="/wp-content/themes/bakerLanding/slices\Baker College Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    <div id="SlideInfo">
      <h1><?php get_feild('headline') ?></h1> 
      <a href="<?php the_feild('play_button_url_') ?>" target="_blank"><img src="/wp-content/themes/bakerLanding/slices/Video Ply BTN.png" alt="Play Video"></a>
      <p><?php the_feild('sub-headline') ?></p>
    </div><!-- close SlideInfo -->
  </header>
</div><!-- close head -->

<!-- mobile form -->
<div class="mobileForm" >
    <div id="Intro">
      <h4><?php the_feild('from_title') ?></h4>
      <p><?php the_feild('form_content') ?></p>
    </div>
    <b><p>Im a form</p></b>
    <p>eventualty</p>
</div>

<div id="middle">
<div class="content">

<!-- Follow form section -->
  <main>
    <img class="headline" src="/wp-content/themes/bakerLanding/slices/Success Starts Here Headline.png">
      <p><?php the_feild('sucess_starts_here_para') ?></p>
      <h5>5 Reasons to enroll</h5>
      	<?php the_feild('reasons_to_enroll') ?>
      <h2><?php the_feild('program_class_heading') ?></h2>

      <div class="hold">
        <img class="left" src="<?php the_feild('1st_program_image') ?>" />
        <h3<?php the_feild('1st_program_title') ?></h3>
        <p><?php the_feild('1st_program_information') ?></p>
      </div>
<?php
  if(get_field('2nd_program_title'))
        {echo '<div class="hold"> <img class="right" src="'. get_field('2nd_program_image') . '" /> <h3>' . get_feild('2nd_program_title') . '</h3> <p>' . get_feild('2nd_program_information') . '</p> </div>';}
  if(get_field('3rd_program_title'))
        {echo '<div class="hold"> <img class="left" src="'. get_field('3rd_program_image') . '" /> <h3>' . get_feild('3rd_program_title') . '</h3> <p>' . get_feild('3rd_program_information') . '?></p> </div>';}
  if(get_field('4th_program_title'))
        {echo '<div class="hold"> <img class="right" src="'. get_field('4th_program_image') . '" /> <h3>' . get_feild('4th_program_title') . '</h3> <p>' . get_feild('4th_program_information') . '?></p> </div>';}
?>
  </main>

  <!-- Follow form -->
  <div class="aside fixme" id="fix" >
      <div id="Intro">
        <h4><?php the_feild('from_title') ?></h4>
        <p><?php the_feild('form_content') ?></p>
      </div>
      <b><p>Im a form</p></b>
      <p>eventualty</p>
  </div>
</div><!-- .content close -->
</div>
<!-- Photo/Instagram tile area-->
<div class="tiles">
  <div id="section">
    <div class="contain">
	<?php
	if(get_field('social_headline_image'))
	{echo '<img src="' . get_field('social_headline_image') . '">';}
	
	if(get_field('social_headline_line_1'))
	{echo '<h3 class="social_headline_line_1">' . get_field('social_headline_line_1') . '</h3>'. '<h3 class="social_headline_line_2">' . get_field('social_headline_line_2') . '</h3>';}
	?>
      <p>Join the thousands of Bakrt studentsand Alumni that are enjoying their well deserved sucess. Being proudly passionate anout something can make yoy feel like and anomoly but that is a community of people who are just as dedicated as you are. The meaningful relationships you make here will go far beyond graduation.</p>
      <h6><?php the_feild('hashtag') ?></h6>
    </div>
  </div>
</div>
<!-- CTA -->
<div class="footerSplash">
  <footer>
    <div class="contain">

	<?php
	if(get_field('cta_headline_image'))
	{echo '<img src="' . get_field('cta_headline_image') . '">';}
	
	if(get_field('social_headline_line_1'))
	{echo '<h3 class="call_to_action_text_line_1">' . get_field('call_to_action_text_line_1') . '</h3>'. '<h3 class="call_to_action_text_line_2">' . get_field('call_to_action_text_line_2') . '</h3>';}
	?>

      <a href="<?php the_feild('cal_to_action_link') ?>" class="button"><?php the_feild('call_to_action_button') ?></a>
  </footer>
</div>
<?php get_footer(); ?>
