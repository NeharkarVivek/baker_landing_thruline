<?php
/**
 * Template Name: New Landing Page Style
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Discover your way forward at Baker</title>

	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'GTM-5QPVDP4':true});</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5JDHH2');</script>
	<!-- End Google Tag Manager -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript” src=”//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->


	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/styles.css">

	<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/stylesLandingV1.css"> -->
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JDHH2"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div id="LandingPage">

	<div class="main-header LanndingPage">
	<div class="row">
		<div class="main-header__container">
			<div class="main-header__search-bar">
				<input type="text" class="main-header__search-input" placeholder="Search">
				<div class="main-header__search-input-close"></div>
			</div>
			<div class="main-header__left-nav">
				<a href="/" class="main-header__logo">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo.svg" alt="" class="main-header__logo-img-red">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo--white.svg" alt="" class="main-header__logo-img-white">
				</a>
			</div>
			<div class="main-header__right-nav">
				<a class="main-header__right-btn main-header__call-btn" href="tel:810-766-2170"></a>
				<a class="main-header__call-text" href="tel:810-766-2170">810-766-2170</a>
			</div>
		</div>
	</div>
</div>

	<div class="LandingPage-fullwidth-video-cta" style="background-image: url('<?php echo get_bloginfo('template_directory');?>/assets/images/Discover Landing Page Hero@2x.jpg');">
	<div class="row">
		<div class="LandingPage-fullwidth-video-cta__wrap">
			<h1 class="LandingPage-fullwidth-video-cta__title">Discover your <br> way forward</h1>
			<div class="LandingPage-fullwidth-video-cta__sub-text">
				<p>As a non-profit, you are our top priority. With over 10 Michigan campuses, online classes, and courses led by professionals, we offer a high-quality education to those looking to change their lives for the better.</p>
			</div>
			<a href="#cform" class="LandingPage-fullwidth-video-cta__cta-btn">
				Get Started Today
			</a>
		</div>
	</div>
</div>

<div class="contact">
<div class="row">
<div class="contact-form-head" id="cform">
	<h5>Get Started Today</h5>
	<h2>Contact our Enrollment Specialists</h2>
	<p>One of our advsiors will be in touch with you shortly!</p>
</div>

<div class="lead-form keypath">
		<form name="ppcForm" id="ppcForm" method=get action="https://webservices.keypathpartners.com/ilm/default.ashx" class="contact-form__form keypath">
			<div class="contact-form__error-message">Errors highlighted in red</div>
			<div class="contact-form__required-text">All Fields Required</div>
			<div class="contact-form__row contact-form__row--two">
				<div class="contact-form__field-wrap">
					<label for="firstname" class="contact-form__label">* First Name</label>
					<input class="text" type='text' name='firstname' id='firstname' placeholder="First Name" required>
				</div>
				<div class="contact-form__field-wrap">
					<label for="lastname" class="contact-form__label">* Last Name</label>
					<input class="text" type='text' name='lastname' id='lastname' placeholder="Last Name" required>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
					<label for="email" class="contact-form__label">Email</label>
					<input name="email" type="email" name='email' id='email' placeholder="Email" required>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
					<label for="dayphone" class="contact-form__label">Phone:</label>
					<input name="dayphone" type='tel' id='dayphone' placeholder="Phone" minlength="10" data-inputmask="'mask': '999-999-9999'" required>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
					<label for="address" class="contact-form__label">Address</label>
					<input class="text" type='text' name='address' id='address' placeholder="Address" required>
				</div>
			</div>
			<div class="contact-form__row contact-form__row--four">
				<div class="contact-form__field-wrap">
					<label for="zip" class="contact-form__label">ZIP Code:</label>
					<input name="zip" type="number" id='zip' placeholder="Zip Code"  minlength="5" data-inputmask="'mask': '99999'" required>
				</div>
				<div class="contact-form__field-wrap">
					<label for="city" class="contact-form__label">City:</label>
					<input name="city" type="text"  id='city' placeholder="City" required>
				</div>
				<div class="contact-form__field-wrap">
					<label for="state" class="contact-form__label">State:</label>
					<select name="state" class="js-select2" style="width: 100%" name="state" id="" data-placeholder="Select a State" required>
						<option value=""></option>
						<option class="dropdownvalue" value="AL">Alabama</option>
						<option class="dropdownvalue" value="AK">Alaska</option>
						<option class="dropdownvalue" value="AZ">Arizona</option>
						<option class="dropdownvalue" value="AR">Arkansas</option>
						<option class="dropdownvalue" value="CA">California</option>
						<option class="dropdownvalue" value="CO">Colorado</option>
						<option class="dropdownvalue" value="CT">Connecticut</option>
						<option class="dropdownvalue" value="DE">Delaware</option>
						<option class="dropdownvalue" value="DC">Dist. of Col.</option>
						<option class="dropdownvalue" value="FL">Florida</option>
						<option class="dropdownvalue" value="GA">Georgia</option>
						<option class="dropdownvalue" value="HI">Hawaii</option>
						<option class="dropdownvalue" value="ID">Idaho</option>
						<option class="dropdownvalue" value="IL">Illinois</option>
						<option class="dropdownvalue" value="IN">Indiana</option>
						<option class="dropdownvalue" value="IA">Iowa</option>
						<option class="dropdownvalue" value="KS">Kansas</option>
						<option class="dropdownvalue" value="KY">Kentucky</option>
						<option class="dropdownvalue" value="LA">Louisiana</option>
						<option class="dropdownvalue" value="ME">Maine</option>
						<option class="dropdownvalue" value="MD">Maryland</option>
						<option class="dropdownvalue" value="MA">Massachusetts</option>
						<option class="dropdownvalue" value="MI">Michigan</option>
						<option class="dropdownvalue" value="MN">Minnesota</option>
						<option class="dropdownvalue" value="MS">Mississippi</option>
						<option class="dropdownvalue" value="MO">Missouri</option>
						<option class="dropdownvalue" value="MT">Montana</option>
						<option class="dropdownvalue" value="NE">Nebraska</option>
						<option class="dropdownvalue" value="NV">Nevada</option>
						<option class="dropdownvalue" value="NH">New Hampshire</option>
						<option class="dropdownvalue" value="NJ">New Jersey</option>
						<option class="dropdownvalue" value="NM">New Mexico</option>
						<option class="dropdownvalue" value="NY">New York</option>
						<option class="dropdownvalue" value="NC">North Carolina</option>
						<option class="dropdownvalue" value="ND">North Dakota</option>
						<option class="dropdownvalue" value="OH">Ohio</option>
						<option class="dropdownvalue" value="OK">Oklahoma</option>
						<option class="dropdownvalue" value="OR">Oregon</option>
						<option class="dropdownvalue" value="PA">Pennsylvania</option>
						<option class="dropdownvalue" value="RI">Rhode Island</option>
						<option class="dropdownvalue" value="SC">South Carolina</option>
						<option class="dropdownvalue" value="SD">South Dakota</option>
						<option class="dropdownvalue" value="TN">Tennessee</option>
						<option class="dropdownvalue" value="TX">Texas</option>
						<option class="dropdownvalue" value="UT">Utah</option>
						<option class="dropdownvalue" value="VT">Vermont</option>
						<option class="dropdownvalue" value="VA">Virginia</option>
						<option class="dropdownvalue" value="WA">Washington</option>
						<option class="dropdownvalue" value="WV">West Virginia</option>
						<option class="dropdownvalue" value="WI">Wisconsin</option>
						<option class="dropdownvalue" value="WY">Wyoming</option>
					</select>
				</div>
				<div class="contact-form__field-wrap">
					<label for="country" class="contact-form__label">Country</label>
					<select name="country" class="js-select2" style="width: 100%" name="state" id="" data-placeholder="Select a Country" required>
						<option value=""></option>
						<option class="dropdownvalue" value="U.S.A.">U.S.A.</option>
						<option class="dropdownvalue" value="Canada">Canada</option>
						<option class="dropdownvalue" value="Outside U.S.A.">Outside U.S.A.</option>
					</select>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
					<label for="gradyear" class="contact-form__label">High School Graduation Year:</label>
					<select class="js-select2" style="width: 100%" name="gradyear" id="" data-placeholder="Graduation Year" required>
						<option value=""></option>
						<option class="dropdownvalue" value="2020">2020</option>
						<option class="dropdownvalue" value="2019">2019</option>
						<option class="dropdownvalue" value="2018">2018</option>
						<option class="dropdownvalue" value="2017">2017</option>
						<option class="dropdownvalue" value="2016">2016</option>
						<option class="dropdownvalue" value="2015">2015</option>
						<option class="dropdownvalue" value="2014">2014</option>
						<option class="dropdownvalue" value="2013">2013</option>
						<option class="dropdownvalue" value="2012">2012</option>
						<option class="dropdownvalue" value="2011">2011</option>
						<option class="dropdownvalue" value="2010">2010</option>
						<option class="dropdownvalue" value="2009">2009</option>
						<option class="dropdownvalue" value="2008">2008</option>
						<option class="dropdownvalue" value="2007">2007</option>
						<option class="dropdownvalue" value="2006">2006</option>
						<option class="dropdownvalue" value="2005">2005</option>
						<option class="dropdownvalue" value="2004">2004</option>
						<option class="dropdownvalue" value="2003">2003</option>
						<option class="dropdownvalue" value="2002">2002</option>
						<option class="dropdownvalue" value="2001">2001</option>
						<option class="dropdownvalue" value="2000">2000</option>
						<option class="dropdownvalue" value="1999">1999</option>
						<option class="dropdownvalue" value="1998">1998</option>
						<option class="dropdownvalue" value="1997">1997</option>
						<option class="dropdownvalue" value="1996">1996</option>
						<option class="dropdownvalue" value="1995">1995</option>
						<option class="dropdownvalue" value="1994">1994</option>
						<option class="dropdownvalue" value="1993">1993</option>
						<option class="dropdownvalue" value="1992">1992</option>
						<option class="dropdownvalue" value="1991">1991</option>
						<option class="dropdownvalue" value="1990">1990</option>
						<option class="dropdownvalue" value="1989">1989</option>
						<option class="dropdownvalue" value="1988">1988</option>
						<option class="dropdownvalue" value="1987">1987</option>
						<option class="dropdownvalue" value="1986">1986</option>
						<option class="dropdownvalue" value="1985">1985</option>
						<option class="dropdownvalue" value="1984">1984</option>
						<option class="dropdownvalue" value="1983">1983</option>
						<option class="dropdownvalue" value="1982">1982</option>
						<option class="dropdownvalue" value="1981">1981</option>
						<option class="dropdownvalue" value="1980">1980</option>
						<option class="dropdownvalue" value="1979">1979</option>
						<option class="dropdownvalue" value="1978">1978</option>
						<option class="dropdownvalue" value="1977">1977</option>
						<option class="dropdownvalue" value="1976">1976</option>
						<option class="dropdownvalue" value="1975">1975</option>
						<option class="dropdownvalue" value="1974">1974</option>
						<option class="dropdownvalue" value="1973">1973</option>
						<option class="dropdownvalue" value="1972">1972</option>
						<option class="dropdownvalue" value="1971">1971</option>
						<option class="dropdownvalue" value="1970">1970</option>
						<option class="dropdownvalue" value="1969">1969</option>
						<option class="dropdownvalue" value="1968">1968</option>
						<option class="dropdownvalue" value="1967">1967</option>
						<option class="dropdownvalue" value="1966">1966</option>
						<option class="dropdownvalue" value="1965">1965</option>
						<option class="dropdownvalue" value="1964">1964</option>
						<option class="dropdownvalue" value="1963">1963</option>
						<option class="dropdownvalue" value="1962">1962</option>
						<option class="dropdownvalue" value="1961">1961</option>
						<option class="dropdownvalue" value="1960">1960</option>
						<option class="dropdownvalue" value="1959">1959</option>
						<option class="dropdownvalue" value="1958">1958</option>
						<option class="dropdownvalue" value="1957">1957</option>
						<option class="dropdownvalue" value="1956">1956</option>
						<option class="dropdownvalue" value="1955">1955</option>
					</select>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
					<label for="LocationID2" class="contact-form__label">Which Campus Are You Interested In?</label>
					<select name="LocationID2" class="js-select2" style="width: 100%" name="LocationID2" id="LocationID2" data-placeholder="Choose a Location" required>
						<option value=""></option>
					</select>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
					<label for="CurriculumID" class="contact-form__label">Which Program Are You Interested In?</label>
					<select name="CurriculumID" class="js-select2" style="width: 100%"  name="CurriculumID" id="CurriculumID"  data-placeholder="Select a Program" required>
						<option value=""></option>
					</select>
				</div>
			</div>

			<input name="" type="submit" class="btn-cta-round btn-cta-round--red contact-form__submit" value="Submit">


			<input type='hidden' placeholder="" name='AffiliateLocationID' id='AffiliateLocationID' value=0 >
			<input type='hidden' placeholder="" name='FormID' id='FormID' value=7459 >
			<input type="hidden" placeholder="" name="LocationID" id="LocationID" value="1286">
			<input type='hidden' placeholder="" name='CampaignID' id='CampaignID' value=7172 >
			<input type='hidden' placeholder="" name='VendorID' id='VendorID' value=38999 >



<div class="hideme" style="display: none !important;">
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
    <input type='hidden' placeholder="" name='CaptureURL' id='CaptureURL' value='<?php echo "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'>
    <input type='hidden' placeholder="" name='ReturnToURL' id="ReturnToURL" value="https://discover.baker.edu/your-way-forward/thank-you/">
    <input type="hidden" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>">
    <input type="hidden" name="SearchEngine" id="SearchEngine" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="SearchEngineCampaign" id="SearchEngineCampaign" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="VendorAccountID" id="VendorAccountID" value="<?php echo $_REQUEST['VendorAccountID']?>">
    <input type='hidden' placeholder="" name='ClientSourceCode' id='ClientSourceCode' value="<?php echo $_REQUEST['ClientSourceCode']?>" >
	<?php endwhile; ?>
<?php endif; ?>
</div>



			<script src="https://webservices.plattformad.com/cfe/JSON.ashx?JSONAction=FormProgramsExtended&amp;formid=7459&amp;CurriculumCategoryID=8" type="text/javascript"></script>
			<script src="https://webservices.plattformad.com/cfe/scripts/JSONObjectCurriculumHandlerCID.js" type="text/javascript"></script>
			<script src="https://tracking.plattformad.com/" type="text/javascript"></script>
			<script type="text/javascript">
        var __ProgramPleaseSelectText = "Select a program";
        var __LocationPleaseSelectText = "Location of interest";

        InitCurriculumDropDown('LocationID2','CurriculumID');

        var x = document.getElementById("LocationID2");
        if (x.removeEventListener) { // For all major browsers, except IE 8 and earlier
        x.removeEventListener("change", FillPrograms);
        } else if (x.detachEvent) {  // For IE 8 and earlier versions
        x.detachEvent("change", FillPrograms);
        }

        document.getElementById("LocationID2").onchange = function(){
        FillPrograms(1286);
        };

      </script>
			<script src="https://tracking.plattformad.com/LeadSourceRuntime.aspx?&amp;&amp;REFERER_SITE=<?php echo get_site_url(); ?>" language="javascript"></script>
			<script src="https://artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js" type="text/javascript"></script>



			<!--<script type="text/javascript" src="https://tracking.plattformad.com/"></script>
			<script type="text/javascript" src="https://artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js"></script>-->
		</form>
	</div>
</div>
</div>
<div class="locations">

<div class="row">
<div class="locations-title">
<h5>For Your Convenience</h5>
<h2> Locations Across Michigan</h2>
</div>

<div class="locations-grid">
	<div class="row">
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Allen Park</h3></div>
			<div class="locations-grid__location-address">4500 Enterprise Drive<br>Allen Park, MI 48101</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Auburn Hills</h3></div>
			<div class="locations-grid__location-address">1500 University Drive<br>Auburn Hills, MI 48326</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Cadillac</h3></div>
			<div class="locations-grid__location-address">9600 E. 13th Street<br>Cadillac, MI 49601</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Clinton Township</h3></div>
			<div class="locations-grid__location-address">34950 Little Mack Avenue<br>Clinton Twp., MI 48035</div>
		</div>
	</div>
	<div class="row">
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Flint</h3></div>
			<div class="locations-grid__location-address">1050 W. Bristol Road<br>Flint, MI 48507</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Jackson</h3></div>
			<div class="locations-grid__location-address">2800 Springport Road<br>Jackson, MI 49202</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Muskegon</h3></div>
			<div class="locations-grid__location-address">1903 Marquette Avenue<br>Muskegon, MI 49442</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Owosso</h3></div>
			<div class="locations-grid__location-address">1020 S. Washington St.<br>Owosso, MI 48867</div>
		</div>
	</div>
</div>

<div class="locations-callout">
	<div class="row">
		<h2>Global Campuses</h2>
		<div class="locations-callout__location">
			<div class="contain">
			<div class="locations-callout__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online.svg"><h3>Center for Graduate Studies</h3></div>
			<div class="locations-callout__location-address">1116 W. Bristol Road<br>Flint, MI 48507</div>
		</div>
		</div>
		<div class="locations-callout__location">
			<div class="contain">
			<div class="locations-callout__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online.svg"><h3>Baker Online</h3></div>
			<div class="locations-callout__location-address">1116 W. Bristol Road<br>Flint, MI 48507</div>
			</div>
		</div>
	</div>
</div>


<div class="Affiliated_Institutions">
	<div class="row">
		<h2>Affiliated Institutions</h2>
		<div class="Affiliated_Institutions__location">
			<img src="https://www.baker.edu/assets/images/affiliated-institution-landing/cim-logo-secondary-nav.svg" />
			<div class="contain cim">
				<div class="Affiliated_Institutions__location-name"><h3>Culinary Institute of Michigan</h3></div>
				<div class="Affiliated_Institutions__location-name"><h4>Muskegon</h4></div>
				<div class="Affiliated_Institutions__location-address">1903 Marquette Avenue<br>Muskegon, MI 49442</div>
			</div>
		</div>
		<div class="Affiliated_Institutions__location">
			<img src="https://www.baker.edu/assets/images/affiliated-institution-landing/cim-logo-secondary-nav.svg" />
			<div class="contain cim">
				<div class="Affiliated_Institutions__location-name"><h3>Culinary Institute of Michigan</h3></div>
				<div class="Affiliated_Institutions__location-name"><h4>Port Huron</h4></div>
				<div class="Affiliated_Institutions__location-address">3402 Lapper Road<br>Port Huron, MI 48060</div>
			</div>
		</div>
		<div class="Affiliated_Institutions__location">
			<img class="svg" src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/ADI_Logo.svg" />
			<div class="contain adi">
				<div class="Affiliated_Institutions__location-name"><h3>Auto/Diesel Institute of Michigan</h3></div>
				<div class="Affiliated_Institutions__location-name"><h4>Owosso</h4></div>
				<div class="Affiliated_Institutions__location-address">1309 S. M-52<br>Owosso, MI 48867</div>
			</div>
		</div>
	</div>
</div>



</div>
</div>




		<footer class="footer-section">
	<div class="footer-section__bottom row">
		<div class="footer-section__logo">
				<img class="logo-icon" src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo--red-white.svg"/>
		</div>
		<p class="footer-section__accredited">Accredited by <span class="footer-section__link_underline"><a href="#">The Higher Learning Commission</a></span>. An  equal opportunity affirmative action institution.
		An approved institution of the <span class="footer-section__link_underline"><a href="#">National Council for State Authorization Reciprocity Agreements (NC-SARA)</a></span>
		and the <span class="footer-section__link_underline"><a href="#">Midwestern Higher Education Compact (MHEC)</a></span>
		</p>
		<div class="footer-section__bottom_rights-reserved"><a href="https://www.baker.edu">Visit baker.edu</a>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;© 2017 All rights reserved. Baker College</div>
	</div>
</footer>

		<div class="fade-screen"></div>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890dcbf83803e36"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/main.js" type="text/javascript" /></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/zip.js" type="text/javascript" /></script>

		<!-- <script src="bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js" type="text/javascript"></script>
		<script src="dist/js/jquery.validate.js" type="text/javascript"></script>
		<script src="dist/js/additional-methods.js" type="text/javascript"></script> -->


		<script src="https://use.typekit.net/jyv0hxv.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<script>

		$.validator.addMethod('customphone', function (value, element) {
		  return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
		}, "Please enter a valid phone number");

		$.validator.methods.email = function( value, element ) {
  	return this.optional( element ) || /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/.test( value );
		}

		$.validator.addMethod("fillProgram", function(value, element, param) {
		  return this.optional(element) || value != param;
		}, "Please specify the program you are interested in");

		$.validator.addMethod("zipValid", function(value, element) {
		  return this.optional(element) || /^\d{5}$/.test(value);
		}, "Please provide a valid zipcode.");

		$.validator.addMethod("nonNumeric", function(value, element) {
		  return this.optional(element) || !value.match(/[0-9]+/);
		},"Only alphabatic characters allowed.");

		Inputmask.extendDefaults({
		  'autoUnmask': true
		});

		$().ready(function() {
			// validate signup form on keyup and submit
			$(".keypath").validate({
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
					},
					dayphone: "customphone",
					address: "required",
					city: {
						required: true,
						nonNumeric: true
					},
					state: "required",
					zip: "zipValid",
					gradyear: "required",
					LocationID: "required",
					CurriculumID: "required",
				},
				messages: {
					firstname: "Please enter your First Name",
					lastname: "Please enter your Last Name",
					email: "Please enter a valid email address",
					dayphone: "Please enter a valid phone number",
					address: "Please enter your Address",
					city: "Please enter your City",
					state: "Please enter your State",
					zip: "Please enter your Zip Code",
					gradyear: "Please enter your Graduation Year",
					LocationID: "Please enter your Preferred campus location",
					CurriculumID: "Please specify the program you are interested in",
				}
			});
		});

		</script>

		</body>
</html>




<?php get_footer(); ?>
