<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after
 */
?>
<?php the_field('extratracking_code'); ?>



<script>
$('input').jvFloat();

$(document).ready(function(){
  $('.Thank .head').css('height', $(window).height() + "px");
  console.log('height added' + $('.Thank .head').height());
  
  

  //$('#SlideInfo h1, .success_holder h2').append('<i> > </i>');
   $('.all_programs > h1').append(' <i>  </i>');

  // All Program in single wysiwig editor - Toggle 
  $('.all_programs > h1').click(function(e){
      var current = $(this).next('blockquote');
      $('.all_programs > blockquote').not(current).hide(100);
      $('.all_programs > h1 i').not($(this).find('i')).removeClass('open_arrow');
      $('.all_programs > h1').not($(this)).removeClass('open'); // open = Mobile  || open_arrow = desktop

      current.slideToggle();
      // open = Mobile  || open_arrow = desktop
      if($(this).find('i').hasClass('open_arrow') || $(this).hasClass('open') ){
          $(this).find('i').removeClass('open_arrow');
          $(this).removeClass('open');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow') || ! $(this).hasClass('open') ) {
          $(this).find('i').addClass('open_arrow');
          $(this).addClass('open')
      }
      if($(window).width() < 767){
                  
      }
  });

  // Separate Program - Toggle Function
  $('.hold > h3').click(function(e){
      var current = $(this).next('section');
      $('.hold > section').not(current).hide(100);
      $('.hold > h3 i').not($(this).find('i')).removeClass('open_arrow');
      current.slideToggle();
      if($(this).find('i').hasClass('open_arrow')){
          $(this).find('i').removeClass('open_arrow');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow')) {
          $(this).find('i').addClass('open_arrow');
      }
  });

  $('.locations_holder .content .title > h3').click(function(e){
    if($(window).width() < 768){
      $('.locations_list, .additional_locations').toggle();
      $(this).find('i').toggleClass('loc_open_arrow');
    }
  })

});


//Dynamic Header height 
    //Adjust element dimensions on resize event
        if($(window).width() > 768){
          $(window).on('resize', function () {
              //$('.head').css('height', $(this).height() + "px");

              var rightSpace = $(window).width() - ($('.content_upper .content').offset().left + $('.content_upper .content').outerWidth());
              var banHeight = $(window).height() - 65 ;
              
          });
        }
        else{
          //$('.head').css('height', $(this).height() + "px");
        }
    

    //Trigger the event
    $(window).trigger('resize');


//Follow form Js
$(window).on('resize', function () {
    
    $('.Thank .head').css('height', $(this).height() + "px");
    formScroll();
});




function formScroll() {
  var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
  var fixmePosition = $('.fixme').position();
  var leftmargin = $('.fixme').css('margin-left');
  //var width = document.getElementById('fix').offsetWidth;
  //var intoSize = $('.intro').height() + 35;
  var intoSize = $('#intro').height();
  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
console.log(collapsePoint);

  //$(window).scroll(function() {   
  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
        var currentScroll = $(window).scrollTop();        // get current position  
        var windowSize = $(window).width();
        var formHeight= $('.fixme').height();
        /*var tilesHeight= $('.tiles').offset().top ;
        var stopMe = tilesHeight - formHeight;*/
        var tilesHeight= $('.testimonial_holder').offset().top ;
        var stopMe = tilesHeight - formHeight;

       
        if (currentScroll >= (fixmeTop - 100) && windowSize > 1000) {    // apply position: fixed if you
          
            var fromTop = $('.fixme').offset().top;
            $('.fixme').css({                                 // scroll to that element or below it
          position: 'fixed',
          top: '80px',
          left: fixmePosition.left  + 'px',
          marginTop: '-' + intoSize + 'px',
          });
        } 
        else {                               // apply position: static
        $('.fixme').css({                      // if you scroll above it
          position:'relative',
          top:'auto',
          left :'auto',
            marginTop: '-' + intoSize + 'px',
          });
        }

        /*if (currentScroll >= collapsePoint && windowSize > 1000) {   
            $('.form_holder form.keypath').slideUp();
        }
        if (currentScroll < collapsePoint && windowSize > 1000) {   
            $('.form_holder form.keypath').slideDown();
        }*/


        if (currentScroll >= (stopMe - 100) && windowSize > 1000) {           // apply position: fixed if you
              $('.fixme').css({                                 // scroll to that element or below it
                  position: 'absolute',
                  /*width: width + 'px',*/
                  top: stopMe + 'px',
                  left: fixmePosition.left + 'px',
                    marginTop: '-' + intoSize + 'px',
                });
        }
  });
console.log (leftmargin);
}


$(document).ready(function(){
    
    formScroll();
    $('#Intro').click(function(){
      $('form.keypath').slideToggle();
      var formState = $("form.keypath");
      var windowSize = $(window).width();

    })
    $('.footerSplash footer .title a.button').click(function(e){
      if( $(window).width() >= 1001 && $('form.keypath').is(':hidden')){
        e.preventDefault();
        //$('body, html').animate({scrollTop: $("#top").offset().top});
        $('form.keypath').slideDown();
      }
    })
});




function formSwap(){
  var getWidth = $(window).width();
  if(getWidth <= 900){
    $('.mobileForm form.keypathMobile').remove();
    $('div.aside .keypath').detach().insertAfter('.mobileForm #Intro');           
  }
  else
  {
    $('.mobileForm .keypath').detach().insertAfter('div.aside #Intro'); 
  }
}
</script>

<script>
$(function(){
  var mobileTop = $('form.keypath').offset().top;
  var mobileHeight = $('form.keypath').height();
  var windowSize = $(window).width();
  var ShowHeightMobile = mobileTop + mobileHeight
  $(window).scroll(function(){
     var currentScroll = $(window).scrollTop()
      if (currentScroll > ShowHeightMobile && windowSize < 1000 ) {
        //$('.goToFormMobile').css('display', 'block');
        //$('.goToFormMobile').addClass("slideUp");
      } else {
        $('.goToFormMobile').css('display', 'none');
        }
        });
  });
</script>

<script>

/* Mobile Keyboard On Detection for Avoid title text merging with request info form */

// Detect Mobile Device
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}; 

$(document).ready(function(){
  
  if(isMobile.any()){

    var _originalSize = $(window).width() + $(window).height();
    var _originalHeadHeight = $(window).height();

      $(window).resize(function(){
        if($(window).width() + $(window).height() != _originalSize){
          console.log("keyboard on");
          //alert('keyboard on'); 
          //$(".head").css("height", _originalHeadHeight + "px");  
        }else{
          console.log("keyboard off");
          //alert('keyboard off');
        }
      });
  }

});

$(document).ready(function(){
  if(/android/i.test(navigator.userAgent.toLowerCase()) == true){
    $("#zip").inputmask('remove');
    $("#dayphone").inputmask('remove');
    $("#zip").attr('maxlength','5');      
    $("#dayphone, #evephone").attr('maxlength','10');
  }else{
    $(":input").inputmask();
  }

  $('#city_temp').hide();
  $('#state_temp').hide(); 
  $('.city_state_wrapper').hide();

  $("#zip").change(function() {
    /*if ($.browser.msie && parseInt($.browser.version, 10) === 7)
      setLocation3(jQuery(this).val());
    else*/
      setLocation4(jQuery(this).val());
  });

  function setLocation3(zip) {
      return setLocation4(zip);  // RMR - getJSON is compatible with IE7
  }

  function setLocation4(zip) {
      var url;
      if(window.location.pathname.indexOf("/dev/") == 0){
        url = window.location.protocol + "//" + window.location.host + "/dev/" + "wp-content/themes/CIMLanding/getZip2.php?zip=" + zip;
      }
      else{
        url = window.location.protocol + "//" + window.location.host + "/wp-content/themes/CIMLanding/getZip2.php?zip=" + zip;
      }
      $.ajax({
              async: false,
              dataType: "json",
              url: url,
              success: function(zip) {
                  try {
                      $("#city").val(zip[0][2]);
                      $("#state").val(zip[0][3]);
                      $("#city_temp").val(zip[0][2]);
                      $("#state_temp").val(zip[0][3]);
                  } catch (e) {
                      if($("#zip").val() !=''){
                        $('.city_state_wrapper').show();
                        $('#enterpriseform #city_temp').css('display','block');
                        $('#enterpriseform #state_temp').css('display','block');
                      }
                  }
              }
          })
          .fail(function() {
            if($("#zip").val() !=''){
              $('.city_state_wrapper').show();
              $('#enterpriseform #city_temp').css('display','block');
              $('#enterpriseform #state_temp').css('display','block');
            }
          });
  }

  $('#state_temp').change(function() {
    var state_s = $('#state_temp').val(); //.find(":selected").text();
    $("#state").val(state_s);
  });
  
  $("#city_temp").change(function() {
    $("#city").val($(this).val());
  });

  if($('body').hasClass('twostep')){
    $('form.keypath').hide();
    $('#Intro > h3').text('Find Your Program');
    $('.step.step2').insertAfter('.step.current');
    $('.step.step2').hide();
    $('.step.current').show();
    $('form.keypath').show();

    $(this).find('.nextstep.button').click(function(){
      if($(this).closest('.keypath').valid()){
        $(this).parents('.step').slideUp().next('.step').slideDown();
      }
      return false;
    });

    $('.stepinfo.goback').click(function(){
      $('.step.current').show();
      $('.step.step2').hide();
    });
  }  
});

function getQueryVariable(variable)
{
var query = window.location.search.substring(1);
var vars = query.split("&");
for (var i=0;i<vars.length;i++) {
  var pair = vars[i].split("=");
  if(pair[0] == variable){return pair[1];}
}
return(false);
} 

var targetSource = getQueryVariable('twostep');
  if(targetSource == "yes"){
  $('body').addClass('twostep');
}

</script>

<?php wp_footer(); ?>

</body>

</html>
