$().ready(function() {

  // validate signup form on keyup and submit
  $(".keypath").validate({
    rules: {
      firstname: "required",
      lastname: "required",
      email: {
        required: true,
        email: true
      },
      dayphone: "required",
      address: "required",
      city: "required",
      state: "required",
      zip: "required",
      gradyear: "required",
    },
    messages: {
      firstname: "Please enter your First Name",
      lastname: "Please enter your Last Name",
      email: "Please enter a valid email address",
      dayphone: "Please enter your phone number",
      address: "Please enter your Address",
      city: "Please enter your City",
      state: "Please enter your State",
      zip: "Please enter your Zip Code",
      gradyear: "Please enter your Graduation Year",
    }
  });
