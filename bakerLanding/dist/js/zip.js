// form validation

function contactFromValidation() {
	function showFormError(state) {
		if(state) {
			$(".contact-form__error-message").show();
		} else if(!state) {
			$(".contact-form__error-message").hide();
		}
	}

	var contactFromValidator = $(".contact-form__form").validate({
		ignore: [],
		rules: {
		    'zip': {
		      required: true,
		      number: true
		    }
		},
		// Validation on focus out.
		onfocusout: function(element) {
			this.element(element)
			if(this.numberOfInvalids() > 0) {
				showFormError(true);
			} else {
				showFormError(false);
			}
		},
		// Error handler on form submit
		invalidHandler: function(event, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				showFormError(true);
			} else {
				showFormError(false);
			}
		},
		// Needed for styling select2 errors.
		errorPlacement: function (error, element) {
			if (element.hasClass('js-select2')) {
		        error.insertAfter(element.next('span'));  // select2
		    } else {
		        error.insertAfter(element);               // default
		    }
		}
	});

	// Validate jquery ui datepicker fields on select
	$('.contact-form .js-popup-datepicker').on('change', function() {
	    $(this).valid();
	    // this covers the random edge case if a user selects only the date, it will show form error. this prevents the error from showing when date is selected.
	    if(contactFromValidator.numberOfInvalids() == 0) {
	    	showFormError(false);
	    }
	});

	// Validate select2 fields on select
	$('.contact-form .js-select2').on('change', function() {
	    $(this).valid();
	});
}

contactFromValidation();


// for populating city and state when zipcode is entered https://www.zipcodeapi.com
function zipCodeApi() {
	$(function() {
		// IMPORTANT: Fill in your client key
		var clientKey = "js-lfqFzMcKP7pzyEn4g6eFeWQv01GPrdPNsfs22ENSMdrlRPp8VDNazEGAGmq7AfU3";

		var cache = {};
		var container = $(".contact-form__form");
		container.find("input[name='zip']").after('<label class="error error--zip"></label>');
		var errorDiv = container.find("label.error");

		/** Handle successful response */
		function handleResp(data)
		{
			// Check for error
			if (data.error_msg)
				errorDiv.text(data.error_msg);
			else if ("city" in data)
			{
				// Set city and state
				container.find("input[name='city']").val(data.city);
				container.find("input[name='state']").val(data.state);
				container.find("select[name='state']").val(data.state).trigger('change');
				container.find("select[name='country']").val('U.S.A.').trigger('change');
			}
		}

		// Set up event handlers
		container.find("input[name='zip']").on("keyup change", function() {
			errorDiv.empty();
			// Get zip code
			var zipcode = $(this).val().substring(0, 5);
			if (zipcode.length == 5 && /^[0-9]+$/.test(zipcode))
			{
				// Clear error
				errorDiv.empty();

				// Check cache
				if (zipcode in cache)
				{
					handleResp(cache[zipcode]);
				}
				else
				{
					// Build url
					var url = "https://www.zipcodeapi.com/rest/"+clientKey+"/info.json/" + zipcode + "/radians";

					// Make AJAX request
					$.ajax({
						"url": url,
						"dataType": "json"
					}).done(function(data) {
						handleResp(data);

						// Store in cache
						cache[zipcode] = data;
					}).fail(function(data) {
						if (data.responseText && (json = $.parseJSON(data.responseText)))
						{
							// Store in cache
							cache[zipcode] = json;

							// Check for error
							if (json.error_msg)
								errorDiv.text(json.error_msg);
						}
						else
							errorDiv.text('Request failed.');
					});
				}
			}
		}).trigger("change");
	});
}
zipCodeApi();
