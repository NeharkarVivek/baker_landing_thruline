<?php
/**
 * The template for displaying the header
 */
?>
<!DOCTYPE html>
<head>
   	 <!-- Set the viewport width to device width for mobile -->
   	 <meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Sass Generated File | Sass file /styles.sass
  	<link href="mobile.css" rel="stylesheet" >-->
  	<!-- Sass Generated File | Sass file /styles.sass -->
  	<link href="<?php echo get_bloginfo('template_directory');?>/css/style2.css" rel="stylesheet">
  	<link href="<?php echo get_bloginfo('template_directory');?>/css/responsive2.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/animations.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.css">
 	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/additional-methods.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.phone.extensions.js"></script>
	<script src="<?php echo get_bloginfo('template_directory');?>/js/jquery.inputmask.js"></script>

  <script src="//cdn.optimizely.com/js/7546423263.js"></script>  

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PFLTQ4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PFLTQ4');</script>
<!-- End Google Tag Manager -->

	<?php wp_head(); ?>
</head>
<body class="<?php echo get_the_title();?>">
