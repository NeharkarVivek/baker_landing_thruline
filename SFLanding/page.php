<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- test custom post type -->

<div class="head" style="background: <?php the_field('big_slide_image'); ?> ;">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="<?php echo get_bloginfo('template_directory');?>/slices/Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    <div id="SlideInfo">

	<div class="VertAlign">
      	<h1><?php the_field('headline'); ?></h1>
       		<?php if(get_field('play_button_url_')): ?>
		 <a href="<?php the_field('play_button_url_'); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/slices/Video Ply BTN.png" alt="Play Video"></a>
		<?php endif; ?>
	<p><?php the_field('sub-headline') ?></p>
	</div>

    </div><!-- close SlideInfo -->
    <div id="phone">
      <p style="font-family: Nexa-regular, sans-serif;"><b><span id="">Toll-Free:</span> <a href="tel:800-931-2694">(800) 931-2694</a></b></p>
    </div><!-- close phone-->
  </header>
</div><!-- close head -->


<!-- mobile form -->
<div class="mobileForm">
<span class="anchor" id="mobileForm"></span>
    <div id="Intro">
      <h4><?php the_field('form_title'); ?></h4>
      <p><?php the_field('form_content'); ?></p>
    </div>
    <?php get_template_part('keypathFormMobile'); ?>
</div>

<div class="goToFormMobile">
   <a href="#mobileForm"><div id="goToFormMobile">
     <h4><?php the_field('form_title'); ?></h4>
     <p>Click here to request information</p>
   </div></a>
</div>


<div id="middle">
<div class="content">

<!-- Follow form section -->
  <main>
    <h2 class="Black">Success Starts <span class="Red">Here</span></h2>
      <?php the_field('sucess_starts_here_para') ?>
      <h5>A New Approach To academic Excellence</h5>
      	<?php the_field('reasons_to_enroll') ?>
      <h2 class="academics">Academics</h2>
	<span><?php the_field('avail_degrees') ?></span>

      <div class="hold">
        <img class="left" src="<?php the_field('1st_program_image') ?>" />
        <h3><?php the_field('1st_program_title'); ?></h3>
        <?php the_field('1st_program_information'); ?>
      </div>

	<?php if(get_field('2nd_program_title')): ?>
        	<div class="hold">
			<img class="right" src=" <?php the_field('2nd_program_image')?>" />
			<h3><?php the_field('2nd_program_title')?> </h3>
			<?php the_field('2nd_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('3rd_program_title')): ?>
        	<div class="hold">
			<img class="left" src="<?php the_field('3rd_program_image')?>" />
			<h3><?php the_field('3rd_program_title')?> </h3>
			<?php the_field('3rd_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('4th_program_title')): ?>
        	<div class="hold">
			<img class="right" src="<?php the_field('4th_program_image')?>" />
			<h3><?php the_field('4th_program_title')?> </h3>
			<?php the_field('4th_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('5th_program_title')): ?>
        	<div class="hold">
			<img class="left" src="<?php the_field('5th_program_image')?>" />
			<h3><?php the_field('5th_program_title')?> </h3>
			<?php the_field('5th_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('6th_program_title')): ?>
        	<div class="hold">
			<img class="right" src="<?php the_field('6th_program_image')?>" />
			<h3><?php the_field('6th_program_title')?> </h3>
			<?php the_field('6th_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('7th_program_title')): ?>
        	<div class="hold">
			<img class="right" src="<?php the_field('7th_program_image')?>" />
			<h3><?php the_field('7th_program_title')?> </h3>
			<?php the_field('7th_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('8th_program_title')): ?>
        	<div class="hold">
			<img class="right" src="<?php the_field('8th_program_image')?>" />
			<h3><?php the_field('8th_program_title')?> </h3>
			<?php the_field('8th_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('9th_program_title')): ?>
        	<div class="hold">
			<img class="right" src="<?php the_field('9th_program_image')?>" />
			<h3><?php the_field('9th_program_title')?> </h3>
			<?php the_field('9th_program_information')?>
		</div>
	<?php endif; ?>
	<?php if(get_field('10th_program_title')): ?>
        	<div class="hold">
			<img class="right" src="<?php the_field('10th_program_image')?>" />
			<h3><?php the_field('10th_program_title')?> </h3>
			<?php the_field('10th_program_information')?>
		</div>
	<?php endif; ?>
  </main>

  <!-- Follow form -->
  <div class="aside fixme" id="fix" >
      <div id="Intro" class="intro">
        <h4><?php the_field('form_title'); ?></h4>
        <p><?php the_field('form_content'); ?></p>
      </div>
	<?php get_template_part('keypathForm'); ?>

</div>
</div><!-- .content close -->
</div>

<!-- Photo/Instagram tile area-->
<div class="tiles">
  <div id="section">
    <div class="contain">
      <h2>A Class Apart</h2>
<p>St. Francis School of Law is the first not-for-profit online law school in the United States.</p>
<p>Unlike colleges that are organized as profit-making enterprises, or subsidized by government funds and alumni donations, St. Francis operates as an independent institution devoted to serving our students and their interests.</p>
<p>As a not-for-profit law school, we have the freedom to invest in <a style="color:#ffffff" href="http://www.stfrancislaw.com/academics/">the quality of education</a> and focus on <a style="color:#ffffff" href="http://www.stfrancislaw.com/about-sfl/core-values/">the core values</a> that form the cornerstone of St. Francis. As a student, you can be confident that our first priority is providing you with a high quality program that prepares you to excel in the field of law.</p>
<p><a style="color:#ffffff" href="http://www.stfrancislaw.com/contact/">Contact us today</a>&nbsp;to learn more about our innovative <a style="color:#ffffff" href="http://www.stfrancislaw.com/academics/juris-doctor/">Juris Doctor program</a>, comprehensive <a style="color:#ffffff" href="http://www.stfrancislaw.com/about-sfl/">student services</a>, and <a style="color:#ffffff" href="http://www.stfrancislaw.com/admissions/tuition/">affordable tuition</a>.</p>
    </div>
  </div>
</div>
<!-- CTA -->
<div class="footerSplash">
  <footer>
    <div class="contain">
	    <h2>Get <span>Started Today</span></h2>
      <a href="<?php the_field('cal_to_action_link') ?>" class="button">GET STARTED</a>
    </div>
  </footer>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
