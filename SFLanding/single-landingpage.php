<?php
/**
 * Template Name: Landing Page for custom post type
 */
get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- test custom post type -->

<div class="head" style="background-image:url(<?php the_field('big_slide_image');?>);">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="<?php echo get_bloginfo('template_directory');?>/slices/Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->

    <!--<div id="phone">
      <p><span id="">Toll-Free:</span> <a href="tel:855-487-7888">855-487-7888</a></p>
      <a href="tel:855-487-7888" class="phone_icon"></a>
    </div> -->
    <!-- close phone-->

    <?php if(get_field('phone_number')): ?>
      <div id="phone">
          <a href="tel:<?php the_field('phone_number');?>" class="phone_icon"><span>Toll-Free:<?php the_field('phone_number');?></span></a>
        </div><!-- close phone-->
    <?php endif; ?> 

    <div id="SlideInfo">
      	<?php if(get_field('headline')): ?>
        <h1><?php the_field('headline'); ?> <i></i></h1>
        <?php endif; ?> 
        <?php if(get_field('sub-headline')): ?>
		      <p><?php the_field('sub-headline') ?></p>
        <?php endif; ?>   
    </div><!-- close SlideInfo -->
    
  </header>
</div><!-- close head -->

<?php if(get_the_title() != 'Thank You'): ?>

<section class="content_upper">
	<div class="content">
	  <div class="form_holder fixme" id="top">
		  	<div id="Intro" class="intro">
		        <h3><?php the_field('form_title'); ?></h3>
		        <p><?php the_field('form_content'); ?></p>
		    </div>
			<?php get_template_part('keypathForm'); ?>
	  </div>
	  <div class="success_holder">
	  	  <?php if(get_field('success_title')): ?>
          <h2><?php the_field('success_title'); ?> </h2>
        <?php endif; ?> 
        <?php if(get_field('video')): ?>
	       <div class="video_holder"><iframe src="https://www.youtube.com/embed/<?php the_field('video') ?>" frameborder="0" allowfullscreen></iframe></div>
        <?php endif; ?> 
	      <?php the_field('sucess_starts_here_para') ?>
	  </div>
	</div>
	
</section>

<!-- mobile form -->


<div class="goToFormMobile">
   <a href="#top"><div id="goToFormMobile">
     <h4><?php the_field('form_title'); ?></h4>
     <p>Click here to request information</p>
   </div></a>
</div>

<?php if(get_field('program_class_heading')): ?>
<div id="middle">
<div class="content">

<!-- Follow form section -->
<main>
      
	<div class="avail_holder">
		<?php if(get_field('program_class_heading')): ?>
			<h2 class="availdegree"><?php the_field('program_class_heading') ?></h2>
		
		    <!--<h2 class="availdegree">Available degrees</h2>-->
		<?php endif; ?>

		<span><?php the_field('avail_degrees') ?></span>
	</div>
	
	<!-- All Programs in Single wsywig Editor -->
	<div class="all_programs">
		<?php the_field('all_programs'); ?>
	</div> 
	<!-- All Programs in Single wsywig Editor END -->
	 
	 <div class="programs_row">
	 <?php if(get_field('1st_program_title')): ?>
	      <div class="hold">
	        <h3><?php the_field('1st_program_title'); ?> <i> </i></h3>
	        <section><?php the_field('1st_program_information'); ?></section>
	      </div>
	<?php endif; ?>
	<?php if(get_field('2nd_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('2nd_program_title')?> <i> </i></h3>
			<section><?php the_field('2nd_program_information')?></section>
		</div>
	<?php endif; ?>
	</div>
	
	<div class="programs_row">
	<?php if(get_field('3rd_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('3rd_program_title')?> <i> </i></h3>
			<section><?php the_field('3rd_program_information')?></section>
		</div>
	<?php endif; ?>
	<?php if(get_field('4th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('4th_program_title')?> <i> </i></h3>
			<section><?php the_field('4th_program_information')?></section>
		</div>
	<?php endif; ?>
	</div>
	<div class="programs_row">
	<?php if(get_field('5th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('5th_program_title')?> <i> </i></h3>
			<section><?php the_field('5th_program_information')?></section>
		</div>
	<?php endif; ?>
	<?php if(get_field('6th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('6th_program_title')?> <i> </i></h3>
			<section><?php the_field('6th_program_information')?></section>
		</div>
	<?php endif; ?>
	</div>
	<div class="programs_row">
	<?php if(get_field('7th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('7th_program_title')?> <i> </i></h3>
			<section><?php the_field('7th_program_information')?></section>
		</div>
	<?php endif; ?>
	<?php if(get_field('8th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('8th_program_title')?> <i> </i></h3>
			<section><?php the_field('8th_program_information')?></section>
		</div>
	<?php endif; ?>
	</div>
	<div class="programs_row">
	<?php if(get_field('9th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('9th_program_title')?> <i> </i></h3>
			<section><?php the_field('9th_program_information')?></section>
		</div>
	<?php endif; ?>
	<?php if(get_field('10th_program_title')): ?>
        	<div class="hold">
			<h3><?php the_field('10th_program_title')?> <i> </i></h3>
			<section><?php the_field('10th_program_information')?></section>
		</div>
	<?php endif; ?>
	</div>

</main>

  <!-- Follow form -->
  
</div><!-- .content close -->
</div>
<?php endif; ?>




<?php
$CampusLocation = array(
    "Allen Park" => "4500 Enterprise Drive </br>Allen Park, MI 48101",
    "Auburn Hills" => "1500 University Drive </br>Auburn Hills, MI 48326",
    "Cass City" => "6667 Main Street </br>Cass City, MI 48726",
    "Cadillac" => "9600 E. 13th Street </br>Cadillac, MI 49601 ",
    "Clinton Township" => "34401 S. Gratiot Avenue </br>Clinton Township, MI 48035",
    "Coldwater" => "70 E. Chicago Street, Suite 380 </br>Coldwater, MI 49036",
    "Flint" => "1050 W. Bristol Road </br>Flint, MI 48507",
    "Fremont" => "5479 W. 72nd Street, Suite 102 </br>Fremont, MI 49412",
    "Jackson" => "2800 Springport Road </br>Jackson, MI 49202",
    "Muskegon" => "1903 Marquette Avenue </br>Muskegon, MI 49442",
    "Owosso" => "1309 S. M-52 </br>Owosso, MI 48867",
    "Port Huron" => "3402 Lapeer Road </br>Port Huron, MI 48060"
);
$AdditionalLocation = array(
    "Baker Center for Graduate Studies" => "1116 W. Bristol Road </br>Flint, MI 48507",
    "Baker Online" => "1116 W. Bristol Road </br>Flint, MI 48507",    
);


?>

<section class="testimonial_holder">
 <?php if(get_field('quote')): ?>
   <div class="content">
     <h3 class="quote"><i>&ldquo;</i><?php the_field('quote')?><i>&rdquo;</i></h3>
     <div class="owner_row">
       <div class="owner_info">
          - <?php the_field('quoter_name')?> <br/>
          <?php the_field('quoter_info')?>
       </div>
     </div>
   </div>
 <?php endif; ?>
 
</section>


<!--<section class="locations_holder" style="background-image:url(<?php the_field('locations_bg_image');?>);">
  <div class="content">
  	  <div class="title"><h3>Locations - Michigan <i>  </i></h3></div> 
  	  <div class="locations_list">
  	  	<?php
  	  		$values = get_field('locations');

  	  		if($values)
  	  		{
  	  			echo '<ul>';

				foreach($values as $value)
				{
					echo '<li><h4>' . $value . '</h4><div>'. $CampusLocation[$value] . '</div></li>';
				}

				echo '</ul>';
			}
  	  	?>


		


  	  </div>
  	  <div class="additional_locations">
		<?php
  	  		$values = get_field('additional_locations');

  	  		if($values)
  	  		{
  	  			echo '<ul>';

				foreach($values as $value)
				{
					echo '<li><h4>' . $value . '</h4><div>'. $AdditionalLocation[$value] . '</div></li>';
				}

				echo '</ul>';
			}
  	  	?>
  	  </div>
  </div>
</section> -->




<div class="copyright">
<h2>Get Started Today!</h2>
<p class="footer_para">Contact St. Francis School of Law now to take the first step toward your legal degree and career.</p>
<p style="text-align:center;">
  © 2017 St. Francis School of Law. All Rights Reserved.<br/>
 <a href="//stfrancislaw.com/disclosure-of-consumer-Information/">Disclosure of Consumer Information</a>  |  <a href="//stfrancislaw.com/refund-policy/">Refund Policy</a> |  <a href="//stfrancislaw.com/privacy-policy/">Privacy  Policy</a>
</p>
</div>


<?php endif; ?> 

<?php endwhile; endif; ?>
<?php get_footer(); ?>
